
package it.polito.dp2.fds.wsdl.fdsinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import it.polito.dp2.fds.wsdl.fdsbasic.StatusEnumType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="flightNumber" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}flightNumberType" minOccurs="0"/>
 *         &lt;element name="deptDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="status" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}statusEnumType" minOccurs="0"/>
 *         &lt;element name="pageNumber" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}pageNumberType"/>
 *         &lt;element name="resultsPerPage" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}resultsPerPageType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flightNumber",
    "deptDate",
    "status",
    "pageNumber",
    "resultsPerPage"
})
@XmlRootElement(name = "getFlightInstances")
public class GetFlightInstances {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String flightNumber;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar deptDate;
    protected StatusEnumType status;
    protected int pageNumber;
    protected int resultsPerPage;

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the deptDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeptDate() {
        return deptDate;
    }

    /**
     * Sets the value of the deptDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeptDate(XMLGregorianCalendar value) {
        this.deptDate = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link StatusEnumType }
     *     
     */
    public StatusEnumType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusEnumType }
     *     
     */
    public void setStatus(StatusEnumType value) {
        this.status = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     */
    public void setPageNumber(int value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the resultsPerPage property.
     * 
     */
    public int getResultsPerPage() {
        return resultsPerPage;
    }

    /**
     * Sets the value of the resultsPerPage property.
     * 
     */
    public void setResultsPerPage(int value) {
        this.resultsPerPage = value;
    }

}
