
package it.polito.dp2.fds.wsdl.fdsinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="flightInstance" type="{http://dp2.polito.it/FDS/wsdl/FDSInfo}flightInstanceNoPassengerListType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flightInstance"
})
@XmlRootElement(name = "getFlightInstanceResponse")
public class GetFlightInstanceResponse {

    @XmlElement(required = true)
    protected FlightInstanceNoPassengerListType flightInstance;

    /**
     * Gets the value of the flightInstance property.
     * 
     * @return
     *     possible object is
     *     {@link FlightInstanceNoPassengerListType }
     *     
     */
    public FlightInstanceNoPassengerListType getFlightInstance() {
        return flightInstance;
    }

    /**
     * Sets the value of the flightInstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightInstanceNoPassengerListType }
     *     
     */
    public void setFlightInstance(FlightInstanceNoPassengerListType value) {
        this.flightInstance = value;
    }

}
