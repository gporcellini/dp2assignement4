
package it.polito.dp2.fds.wsdl.fdsinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="deptIATAcode" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}IATAcodeType" minOccurs="0"/>
 *         &lt;element name="destIATAcode" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}IATAcodeType" minOccurs="0"/>
 *         &lt;element name="deptTime" type="{http://www.w3.org/2001/XMLSchema}time" minOccurs="0"/>
 *         &lt;element name="pageNumber" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}pageNumberType"/>
 *         &lt;element name="resultsPerPage" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}resultsPerPageType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deptIATAcode",
    "destIATAcode",
    "deptTime",
    "pageNumber",
    "resultsPerPage"
})
@XmlRootElement(name = "getFlights")
public class GetFlights {

    protected String deptIATAcode;
    protected String destIATAcode;
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar deptTime;
    protected int pageNumber;
    protected int resultsPerPage;

    /**
     * Gets the value of the deptIATAcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeptIATAcode() {
        return deptIATAcode;
    }

    /**
     * Sets the value of the deptIATAcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeptIATAcode(String value) {
        this.deptIATAcode = value;
    }

    /**
     * Gets the value of the destIATAcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestIATAcode() {
        return destIATAcode;
    }

    /**
     * Sets the value of the destIATAcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestIATAcode(String value) {
        this.destIATAcode = value;
    }

    /**
     * Gets the value of the deptTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeptTime() {
        return deptTime;
    }

    /**
     * Sets the value of the deptTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeptTime(XMLGregorianCalendar value) {
        this.deptTime = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     */
    public void setPageNumber(int value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the resultsPerPage property.
     * 
     */
    public int getResultsPerPage() {
        return resultsPerPage;
    }

    /**
     * Sets the value of the resultsPerPage property.
     * 
     */
    public void setResultsPerPage(int value) {
        this.resultsPerPage = value;
    }

}
