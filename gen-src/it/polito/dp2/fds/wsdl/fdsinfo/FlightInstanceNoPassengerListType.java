
package it.polito.dp2.fds.wsdl.fdsinfo;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import it.polito.dp2.fds.wsdl.fdsbasic.StatusEnumType;


/**
 * <p>Java class for flightInstanceNoPassengerListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="flightInstanceNoPassengerListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="deptDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="gate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="delay" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}delayType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="flightNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="aircraftId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="status" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}statusEnumType" default="BOOKING" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "flightInstanceNoPassengerListType", propOrder = {
    "deptDate",
    "gate",
    "delay"
})
public class FlightInstanceNoPassengerListType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar deptDate;
    protected String gate;
    @XmlElement(required = true)
    protected BigInteger delay;
    @XmlAttribute(name = "flightNumber", required = true)
    protected String flightNumber;
    @XmlAttribute(name = "aircraftId")
    protected String aircraftId;
    @XmlAttribute(name = "status")
    protected StatusEnumType status;

    /**
     * Gets the value of the deptDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeptDate() {
        return deptDate;
    }

    /**
     * Sets the value of the deptDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeptDate(XMLGregorianCalendar value) {
        this.deptDate = value;
    }

    /**
     * Gets the value of the gate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGate() {
        return gate;
    }

    /**
     * Sets the value of the gate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGate(String value) {
        this.gate = value;
    }

    /**
     * Gets the value of the delay property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDelay() {
        return delay;
    }

    /**
     * Sets the value of the delay property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDelay(BigInteger value) {
        this.delay = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the aircraftId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAircraftId() {
        return aircraftId;
    }

    /**
     * Sets the value of the aircraftId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAircraftId(String value) {
        this.aircraftId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link StatusEnumType }
     *     
     */
    public StatusEnumType getStatus() {
        if (status == null) {
            return StatusEnumType.BOOKING;
        } else {
            return status;
        }
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusEnumType }
     *     
     */
    public void setStatus(StatusEnumType value) {
        this.status = value;
    }

}
