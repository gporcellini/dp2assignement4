/**
 * 
 * 		FDSControl service that allows retrieving
 * 		informations from a FDSInfo.xsd database
 * 	
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://dp2.polito.it/FDS/wsdl/FDSInfo")
package it.polito.dp2.fds.wsdl.fdsinfo;
