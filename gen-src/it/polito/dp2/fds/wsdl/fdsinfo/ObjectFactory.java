
package it.polito.dp2.fds.wsdl.fdsinfo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.polito.dp2.fds.wsdl.fdsinfo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPassengerList_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSInfo", "getPassengerList");
    private final static QName _GetPassengerListResponse_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSInfo", "getPassengerListResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.polito.dp2.fds.wsdl.fdsinfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAircrafts }
     * 
     */
    public GetAircrafts createGetAircrafts() {
        return new GetAircrafts();
    }

    /**
     * Create an instance of {@link GetFlightResponse }
     * 
     */
    public GetFlightResponse createGetFlightResponse() {
        return new GetFlightResponse();
    }

    /**
     * Create an instance of {@link GetFlightsResponse }
     * 
     */
    public GetFlightsResponse createGetFlightsResponse() {
        return new GetFlightsResponse();
    }

    /**
     * Create an instance of {@link GetPassengerList }
     * 
     */
    public GetPassengerList createGetPassengerList() {
        return new GetPassengerList();
    }

    /**
     * Create an instance of {@link GetPassengerListResponse }
     * 
     */
    public GetPassengerListResponse createGetPassengerListResponse() {
        return new GetPassengerListResponse();
    }

    /**
     * Create an instance of {@link GetFlights }
     * 
     */
    public GetFlights createGetFlights() {
        return new GetFlights();
    }

    /**
     * Create an instance of {@link GetFlight }
     * 
     */
    public GetFlight createGetFlight() {
        return new GetFlight();
    }

    /**
     * Create an instance of {@link GetFlightInstances }
     * 
     */
    public GetFlightInstances createGetFlightInstances() {
        return new GetFlightInstances();
    }

    /**
     * Create an instance of {@link GetAircraftsResponse }
     * 
     */
    public GetAircraftsResponse createGetAircraftsResponse() {
        return new GetAircraftsResponse();
    }

    /**
     * Create an instance of {@link GetFlightInstancesResponse }
     * 
     */
    public GetFlightInstancesResponse createGetFlightInstancesResponse() {
        return new GetFlightInstancesResponse();
    }

    /**
     * Create an instance of {@link FlightInstanceNoPassengerListType }
     * 
     */
    public FlightInstanceNoPassengerListType createFlightInstanceNoPassengerListType() {
        return new FlightInstanceNoPassengerListType();
    }

    /**
     * Create an instance of {@link InvalidParametersFault }
     * 
     */
    public InvalidParametersFault createInvalidParametersFault() {
        return new InvalidParametersFault();
    }

    /**
     * Create an instance of {@link GetFlightInstanceResponse }
     * 
     */
    public GetFlightInstanceResponse createGetFlightInstanceResponse() {
        return new GetFlightInstanceResponse();
    }

    /**
     * Create an instance of {@link GetFlightInstance }
     * 
     */
    public GetFlightInstance createGetFlightInstance() {
        return new GetFlightInstance();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPassengerList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSInfo", name = "getPassengerList")
    public JAXBElement<GetPassengerList> createGetPassengerList(GetPassengerList value) {
        return new JAXBElement<GetPassengerList>(_GetPassengerList_QNAME, GetPassengerList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPassengerListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSInfo", name = "getPassengerListResponse")
    public JAXBElement<GetPassengerListResponse> createGetPassengerListResponse(GetPassengerListResponse value) {
        return new JAXBElement<GetPassengerListResponse>(_GetPassengerListResponse_QNAME, GetPassengerListResponse.class, null, value);
    }

}
