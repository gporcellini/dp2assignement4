/**
 * 
 * 		FDSControl service that allows check-in boarding
 * 		and managing operations on a FDSInfo.xsd database
 * 	
 * 		FDSControl service that allows check-in boarding
 * 		and managing operations on a FDSInfo.xsd database
 * 	
 * 		FDSControl service that allows check-in boarding
 * 		and managing operations on a FDSInfo.xsd database
 * 	
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package it.polito.dp2.fds.wsdl.fdscontrol;
