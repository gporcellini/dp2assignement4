
package it.polito.dp2.fds.wsdl.fdscontrol;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * Interface for controlling boarding operations flight instances
 * 		
 * 
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "FDSControl", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
@XmlSeeAlso({
    it.polito.dp2.fds.wsdl.fdsbasic.ObjectFactory.class,
    it.polito.dp2.fds.wsdl.fdscontrol.ObjectFactory.class
})
public interface FDSControl {


    /**
     * Operation for getting the list of boarded
     * 				passengers for a flight instance.
     * 			
     * 
     * @param passenger
     * @param resultsPerPage
     * @param deptDate
     * @param pageNumber
     * @param morePages
     * @param flightNumber
     * @throws InvalidFlightInstanceFault_Exception
     * @throws InvalidFlightInstanceStatusFault_Exception
     * @throws InvalidRequestFormatFault_Exception
     */
    @WebMethod(action = "http://dp2.polito.it/dp2/FDS/getBoardedPassengerList")
    @RequestWrapper(localName = "getBoardedPassengerList", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", className = "it.polito.dp2.fds.wsdl.fdscontrol.GetBoardedPassengerList")
    @ResponseWrapper(localName = "getBoardedPassengerListResponse", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", className = "it.polito.dp2.fds.wsdl.fdscontrol.GetBoardedPassengerListResponse")
    public void getBoardedPassengerList(
        @WebParam(name = "flightNumber", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
        String flightNumber,
        @WebParam(name = "deptDate", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
        XMLGregorianCalendar deptDate,
        @WebParam(name = "pageNumber", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
        int pageNumber,
        @WebParam(name = "resultsPerPage", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
        int resultsPerPage,
        @WebParam(name = "passenger", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", mode = WebParam.Mode.OUT)
        Holder<List<it.polito.dp2.fds.wsdl.fdscontrol.GetBoardedPassengerListResponse.Passenger>> passenger,
        @WebParam(name = "morePages", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", mode = WebParam.Mode.OUT)
        Holder<Boolean> morePages)
        throws InvalidFlightInstanceFault_Exception, InvalidFlightInstanceStatusFault_Exception, InvalidRequestFormatFault_Exception
    ;

    /**
     * Operation for boarding a passenger on a flight
     * 				instance.
     * 			
     * 
     * @param deptDate
     * @param passengerName
     * @param flightNumber
     * @throws InvalidFlightInstanceFault_Exception
     * @throws InvalidPassengerStatusFault_Exception
     * @throws InvalidPassengerNameFault_Exception
     * @throws InvalidFlightInstanceStatusFault_Exception
     * @throws InvalidRequestFormatFault_Exception
     */
    @WebMethod(action = "http://dp2.polito.it/dp2/FDS/board")
    @RequestWrapper(localName = "board", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", className = "it.polito.dp2.fds.wsdl.fdscontrol.Board")
    @ResponseWrapper(localName = "boardResponse", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", className = "it.polito.dp2.fds.wsdl.fdscontrol.BoardResponse")
    public void board(
        @WebParam(name = "flightNumber", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
        String flightNumber,
        @WebParam(name = "deptDate", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
        XMLGregorianCalendar deptDate,
        @WebParam(name = "passengerName", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
        String passengerName)
        throws InvalidFlightInstanceFault_Exception, InvalidFlightInstanceStatusFault_Exception, InvalidPassengerNameFault_Exception, InvalidPassengerStatusFault_Exception, InvalidRequestFormatFault_Exception
    ;

    /**
     * Operation for starting the boarding process of a flight
     * 				instance.
     * 			
     * 
     * @param deptDate
     * @param flightNumber
     * @throws InvalidFlightInstanceFault_Exception
     * @throws InvalidFlightInstanceStatusFault_Exception
     * @throws InvalidRequestFormatFault_Exception
     */
    @WebMethod(action = "http://dp2.polito.it/dp2/FDS/startBoarding")
    @RequestWrapper(localName = "startBoarding", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", className = "it.polito.dp2.fds.wsdl.fdscontrol.StartBoarding")
    @ResponseWrapper(localName = "startBoardingResponse", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", className = "it.polito.dp2.fds.wsdl.fdscontrol.StartBoardingResponse")
    public void startBoarding(
        @WebParam(name = "flightNumber", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
        String flightNumber,
        @WebParam(name = "deptDate", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
        XMLGregorianCalendar deptDate)
        throws InvalidFlightInstanceFault_Exception, InvalidFlightInstanceStatusFault_Exception, InvalidRequestFormatFault_Exception
    ;

}
