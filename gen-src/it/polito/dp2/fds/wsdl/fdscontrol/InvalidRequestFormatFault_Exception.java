
package it.polito.dp2.fds.wsdl.fdscontrol;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "InvalidRequestFormatFault", targetNamespace = "http://dp2.polito.it/FDS/wsdl/FDSControl")
public class InvalidRequestFormatFault_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private InvalidRequestFormatFault faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public InvalidRequestFormatFault_Exception(String message, InvalidRequestFormatFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public InvalidRequestFormatFault_Exception(String message, InvalidRequestFormatFault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: it.polito.dp2.fds.wsdl.fdscontrol.InvalidRequestFormatFault
     */
    public InvalidRequestFormatFault getFaultInfo() {
        return faultInfo;
    }

}
