
package it.polito.dp2.fds.wsdl.fdscontrol;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.polito.dp2.fds.wsdl.fdscontrol package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CancelFlightInstance_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "cancelFlightInstance");
    private final static QName _InvalidFlightInstanceStatusFault_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "InvalidFlightInstanceStatusFault");
    private final static QName _SetFlightInstanceDelayResponse_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "setFlightInstanceDelayResponse");
    private final static QName _GetBoardedPassengerListResponse_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "getBoardedPassengerListResponse");
    private final static QName _FlightInstanceNotBoardingFault_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "FlightInstanceNotBoardingFault");
    private final static QName _StartBoarding_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "startBoarding");
    private final static QName _GetBoardedPassengerList_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "getBoardedPassengerList");
    private final static QName _BoardResponse_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "boardResponse");
    private final static QName _InvalidPassengerStatusFault_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "InvalidPassengerStatusFault");
    private final static QName _SetFlightInstanceGate_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "setFlightInstanceGate");
    private final static QName _Board_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "board");
    private final static QName _CheckIn_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "checkIn");
    private final static QName _InvalidRequestFormatFault_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "InvalidRequestFormatFault");
    private final static QName _CancelFlightInstanceResponse_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "cancelFlightInstanceResponse");
    private final static QName _CheckInResponse_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "checkInResponse");
    private final static QName _InvalidFlightInstanceFault_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "InvalidFlightInstanceFault");
    private final static QName _StartBoardingResponse_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "startBoardingResponse");
    private final static QName _SetFlightInstanceGateResponse_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "setFlightInstanceGateResponse");
    private final static QName _SetFlightInstanceDelay_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "setFlightInstanceDelay");
    private final static QName _InvalidPassengerNameFault_QNAME = new QName("http://dp2.polito.it/FDS/wsdl/FDSControl", "InvalidPassengerNameFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.polito.dp2.fds.wsdl.fdscontrol
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBoardedPassengerListResponse }
     * 
     */
    public GetBoardedPassengerListResponse createGetBoardedPassengerListResponse() {
        return new GetBoardedPassengerListResponse();
    }

    /**
     * Create an instance of {@link CancelFlightInstance }
     * 
     */
    public CancelFlightInstance createCancelFlightInstance() {
        return new CancelFlightInstance();
    }

    /**
     * Create an instance of {@link InvalidFlightInstanceStatusFault }
     * 
     */
    public InvalidFlightInstanceStatusFault createInvalidFlightInstanceStatusFault() {
        return new InvalidFlightInstanceStatusFault();
    }

    /**
     * Create an instance of {@link SetFlightInstanceDelayResponse }
     * 
     */
    public SetFlightInstanceDelayResponse createSetFlightInstanceDelayResponse() {
        return new SetFlightInstanceDelayResponse();
    }

    /**
     * Create an instance of {@link FlightInstanceNotBoardingFault }
     * 
     */
    public FlightInstanceNotBoardingFault createFlightInstanceNotBoardingFault() {
        return new FlightInstanceNotBoardingFault();
    }

    /**
     * Create an instance of {@link StartBoarding }
     * 
     */
    public StartBoarding createStartBoarding() {
        return new StartBoarding();
    }

    /**
     * Create an instance of {@link GetBoardedPassengerList }
     * 
     */
    public GetBoardedPassengerList createGetBoardedPassengerList() {
        return new GetBoardedPassengerList();
    }

    /**
     * Create an instance of {@link BoardResponse }
     * 
     */
    public BoardResponse createBoardResponse() {
        return new BoardResponse();
    }

    /**
     * Create an instance of {@link SetFlightInstanceGate }
     * 
     */
    public SetFlightInstanceGate createSetFlightInstanceGate() {
        return new SetFlightInstanceGate();
    }

    /**
     * Create an instance of {@link Board }
     * 
     */
    public Board createBoard() {
        return new Board();
    }

    /**
     * Create an instance of {@link InvalidPassengerStatusFault }
     * 
     */
    public InvalidPassengerStatusFault createInvalidPassengerStatusFault() {
        return new InvalidPassengerStatusFault();
    }

    /**
     * Create an instance of {@link InvalidRequestFormatFault }
     * 
     */
    public InvalidRequestFormatFault createInvalidRequestFormatFault() {
        return new InvalidRequestFormatFault();
    }

    /**
     * Create an instance of {@link CheckIn }
     * 
     */
    public CheckIn createCheckIn() {
        return new CheckIn();
    }

    /**
     * Create an instance of {@link CancelFlightInstanceResponse }
     * 
     */
    public CancelFlightInstanceResponse createCancelFlightInstanceResponse() {
        return new CancelFlightInstanceResponse();
    }

    /**
     * Create an instance of {@link CheckInResponse }
     * 
     */
    public CheckInResponse createCheckInResponse() {
        return new CheckInResponse();
    }

    /**
     * Create an instance of {@link SetFlightInstanceGateResponse }
     * 
     */
    public SetFlightInstanceGateResponse createSetFlightInstanceGateResponse() {
        return new SetFlightInstanceGateResponse();
    }

    /**
     * Create an instance of {@link StartBoardingResponse }
     * 
     */
    public StartBoardingResponse createStartBoardingResponse() {
        return new StartBoardingResponse();
    }

    /**
     * Create an instance of {@link InvalidFlightInstanceFault }
     * 
     */
    public InvalidFlightInstanceFault createInvalidFlightInstanceFault() {
        return new InvalidFlightInstanceFault();
    }

    /**
     * Create an instance of {@link SetFlightInstanceDelay }
     * 
     */
    public SetFlightInstanceDelay createSetFlightInstanceDelay() {
        return new SetFlightInstanceDelay();
    }

    /**
     * Create an instance of {@link InvalidPassengerNameFault }
     * 
     */
    public InvalidPassengerNameFault createInvalidPassengerNameFault() {
        return new InvalidPassengerNameFault();
    }

    /**
     * Create an instance of {@link GetBoardedPassengerListResponse.Passenger }
     * 
     */
    public GetBoardedPassengerListResponse.Passenger createGetBoardedPassengerListResponsePassenger() {
        return new GetBoardedPassengerListResponse.Passenger();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelFlightInstance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "cancelFlightInstance")
    public JAXBElement<CancelFlightInstance> createCancelFlightInstance(CancelFlightInstance value) {
        return new JAXBElement<CancelFlightInstance>(_CancelFlightInstance_QNAME, CancelFlightInstance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidFlightInstanceStatusFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "InvalidFlightInstanceStatusFault")
    public JAXBElement<InvalidFlightInstanceStatusFault> createInvalidFlightInstanceStatusFault(InvalidFlightInstanceStatusFault value) {
        return new JAXBElement<InvalidFlightInstanceStatusFault>(_InvalidFlightInstanceStatusFault_QNAME, InvalidFlightInstanceStatusFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFlightInstanceDelayResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "setFlightInstanceDelayResponse")
    public JAXBElement<SetFlightInstanceDelayResponse> createSetFlightInstanceDelayResponse(SetFlightInstanceDelayResponse value) {
        return new JAXBElement<SetFlightInstanceDelayResponse>(_SetFlightInstanceDelayResponse_QNAME, SetFlightInstanceDelayResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBoardedPassengerListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "getBoardedPassengerListResponse")
    public JAXBElement<GetBoardedPassengerListResponse> createGetBoardedPassengerListResponse(GetBoardedPassengerListResponse value) {
        return new JAXBElement<GetBoardedPassengerListResponse>(_GetBoardedPassengerListResponse_QNAME, GetBoardedPassengerListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FlightInstanceNotBoardingFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "FlightInstanceNotBoardingFault")
    public JAXBElement<FlightInstanceNotBoardingFault> createFlightInstanceNotBoardingFault(FlightInstanceNotBoardingFault value) {
        return new JAXBElement<FlightInstanceNotBoardingFault>(_FlightInstanceNotBoardingFault_QNAME, FlightInstanceNotBoardingFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartBoarding }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "startBoarding")
    public JAXBElement<StartBoarding> createStartBoarding(StartBoarding value) {
        return new JAXBElement<StartBoarding>(_StartBoarding_QNAME, StartBoarding.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBoardedPassengerList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "getBoardedPassengerList")
    public JAXBElement<GetBoardedPassengerList> createGetBoardedPassengerList(GetBoardedPassengerList value) {
        return new JAXBElement<GetBoardedPassengerList>(_GetBoardedPassengerList_QNAME, GetBoardedPassengerList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BoardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "boardResponse")
    public JAXBElement<BoardResponse> createBoardResponse(BoardResponse value) {
        return new JAXBElement<BoardResponse>(_BoardResponse_QNAME, BoardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidPassengerStatusFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "InvalidPassengerStatusFault")
    public JAXBElement<InvalidPassengerStatusFault> createInvalidPassengerStatusFault(InvalidPassengerStatusFault value) {
        return new JAXBElement<InvalidPassengerStatusFault>(_InvalidPassengerStatusFault_QNAME, InvalidPassengerStatusFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFlightInstanceGate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "setFlightInstanceGate")
    public JAXBElement<SetFlightInstanceGate> createSetFlightInstanceGate(SetFlightInstanceGate value) {
        return new JAXBElement<SetFlightInstanceGate>(_SetFlightInstanceGate_QNAME, SetFlightInstanceGate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Board }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "board")
    public JAXBElement<Board> createBoard(Board value) {
        return new JAXBElement<Board>(_Board_QNAME, Board.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckIn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "checkIn")
    public JAXBElement<CheckIn> createCheckIn(CheckIn value) {
        return new JAXBElement<CheckIn>(_CheckIn_QNAME, CheckIn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidRequestFormatFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "InvalidRequestFormatFault")
    public JAXBElement<InvalidRequestFormatFault> createInvalidRequestFormatFault(InvalidRequestFormatFault value) {
        return new JAXBElement<InvalidRequestFormatFault>(_InvalidRequestFormatFault_QNAME, InvalidRequestFormatFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelFlightInstanceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "cancelFlightInstanceResponse")
    public JAXBElement<CancelFlightInstanceResponse> createCancelFlightInstanceResponse(CancelFlightInstanceResponse value) {
        return new JAXBElement<CancelFlightInstanceResponse>(_CancelFlightInstanceResponse_QNAME, CancelFlightInstanceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckInResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "checkInResponse")
    public JAXBElement<CheckInResponse> createCheckInResponse(CheckInResponse value) {
        return new JAXBElement<CheckInResponse>(_CheckInResponse_QNAME, CheckInResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidFlightInstanceFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "InvalidFlightInstanceFault")
    public JAXBElement<InvalidFlightInstanceFault> createInvalidFlightInstanceFault(InvalidFlightInstanceFault value) {
        return new JAXBElement<InvalidFlightInstanceFault>(_InvalidFlightInstanceFault_QNAME, InvalidFlightInstanceFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartBoardingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "startBoardingResponse")
    public JAXBElement<StartBoardingResponse> createStartBoardingResponse(StartBoardingResponse value) {
        return new JAXBElement<StartBoardingResponse>(_StartBoardingResponse_QNAME, StartBoardingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFlightInstanceGateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "setFlightInstanceGateResponse")
    public JAXBElement<SetFlightInstanceGateResponse> createSetFlightInstanceGateResponse(SetFlightInstanceGateResponse value) {
        return new JAXBElement<SetFlightInstanceGateResponse>(_SetFlightInstanceGateResponse_QNAME, SetFlightInstanceGateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFlightInstanceDelay }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "setFlightInstanceDelay")
    public JAXBElement<SetFlightInstanceDelay> createSetFlightInstanceDelay(SetFlightInstanceDelay value) {
        return new JAXBElement<SetFlightInstanceDelay>(_SetFlightInstanceDelay_QNAME, SetFlightInstanceDelay.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidPassengerNameFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dp2.polito.it/FDS/wsdl/FDSControl", name = "InvalidPassengerNameFault")
    public JAXBElement<InvalidPassengerNameFault> createInvalidPassengerNameFault(InvalidPassengerNameFault value) {
        return new JAXBElement<InvalidPassengerNameFault>(_InvalidPassengerNameFault_QNAME, InvalidPassengerNameFault.class, null, value);
    }

}
