
package it.polito.dp2.fds.wsdl.fdscontrol;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import it.polito.dp2.fds.wsdl.fdsbasic.StatusEnumType;


/**
 * <p>Java class for InvalidFlightInstanceStatusFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvalidFlightInstanceStatusFault">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExpectedStatus" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}statusEnumType" maxOccurs="unbounded"/>
 *         &lt;element name="CurrentStatus" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}statusEnumType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvalidFlightInstanceStatusFault", propOrder = {
    "expectedStatus",
    "currentStatus"
})
public class InvalidFlightInstanceStatusFault {

    @XmlElement(name = "ExpectedStatus", required = true)
    protected List<StatusEnumType> expectedStatus;
    @XmlElement(name = "CurrentStatus", required = true)
    protected StatusEnumType currentStatus;

    /**
     * Gets the value of the expectedStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expectedStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpectedStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusEnumType }
     * 
     * 
     */
    public List<StatusEnumType> getExpectedStatus() {
        if (expectedStatus == null) {
            expectedStatus = new ArrayList<StatusEnumType>();
        }
        return this.expectedStatus;
    }

    /**
     * Gets the value of the currentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link StatusEnumType }
     *     
     */
    public StatusEnumType getCurrentStatus() {
        return currentStatus;
    }

    /**
     * Sets the value of the currentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusEnumType }
     *     
     */
    public void setCurrentStatus(StatusEnumType value) {
        this.currentStatus = value;
    }

}
