
package it.polito.dp2.fds.wsdl.fdsbasic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for flightType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="flightType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="deptIATAcode" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}IATAcodeType"/>
 *         &lt;element name="destIATAcode" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}IATAcodeType"/>
 *         &lt;element name="deptTime" type="{http://www.w3.org/2001/XMLSchema}time"/>
 *       &lt;/sequence>
 *       &lt;attribute name="number" use="required" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}flightNumberType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "flightType", propOrder = {
    "deptIATAcode",
    "destIATAcode",
    "deptTime"
})
public class FlightType {

    @XmlElement(required = true)
    protected String deptIATAcode;
    @XmlElement(required = true)
    protected String destIATAcode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar deptTime;
    @XmlAttribute(name = "number", namespace = "http://dp2.polito.it/FDS/wsdl/FDSBasic", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String number;

    /**
     * Gets the value of the deptIATAcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeptIATAcode() {
        return deptIATAcode;
    }

    /**
     * Sets the value of the deptIATAcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeptIATAcode(String value) {
        this.deptIATAcode = value;
    }

    /**
     * Gets the value of the destIATAcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestIATAcode() {
        return destIATAcode;
    }

    /**
     * Sets the value of the destIATAcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestIATAcode(String value) {
        this.destIATAcode = value;
    }

    /**
     * Gets the value of the deptTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeptTime() {
        return deptTime;
    }

    /**
     * Sets the value of the deptTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeptTime(XMLGregorianCalendar value) {
        this.deptTime = value;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

}
