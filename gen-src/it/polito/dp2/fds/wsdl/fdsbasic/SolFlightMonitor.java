
package it.polito.dp2.fds.wsdl.fdsbasic;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aircraft" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}aircraftType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="flight" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}flightType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="flightInstance" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="deptDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="gate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="delay" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}delayType"/>
 *                   &lt;element name="passenger" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}passengerType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="flightNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *                 &lt;attribute name="aircraftId" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
 *                 &lt;attribute name="status" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}statusEnumType" default="BOOKING" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "aircraft",
    "flight",
    "flightInstance"
})
@XmlRootElement(name = "solFlightMonitor")
public class SolFlightMonitor {

    protected List<AircraftType> aircraft;
    protected List<FlightType> flight;
    protected List<SolFlightMonitor.FlightInstance> flightInstance;

    /**
     * Gets the value of the aircraft property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aircraft property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAircraft().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AircraftType }
     * 
     * 
     */
    public List<AircraftType> getAircraft() {
        if (aircraft == null) {
            aircraft = new ArrayList<AircraftType>();
        }
        return this.aircraft;
    }

    /**
     * Gets the value of the flight property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flight property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlight().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlightType }
     * 
     * 
     */
    public List<FlightType> getFlight() {
        if (flight == null) {
            flight = new ArrayList<FlightType>();
        }
        return this.flight;
    }

    /**
     * Gets the value of the flightInstance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flightInstance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlightInstance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SolFlightMonitor.FlightInstance }
     * 
     * 
     */
    public List<SolFlightMonitor.FlightInstance> getFlightInstance() {
        if (flightInstance == null) {
            flightInstance = new ArrayList<SolFlightMonitor.FlightInstance>();
        }
        return this.flightInstance;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="deptDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="gate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="delay" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}delayType"/>
     *         &lt;element name="passenger" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}passengerType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="flightNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
     *       &lt;attribute name="aircraftId" type="{http://www.w3.org/2001/XMLSchema}IDREF" />
     *       &lt;attribute name="status" type="{http://dp2.polito.it/FDS/wsdl/FDSBasic}statusEnumType" default="BOOKING" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "deptDate",
        "gate",
        "delay",
        "passenger"
    })
    public static class FlightInstance {

        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar deptDate;
        protected String gate;
        @XmlElement(required = true)
        protected BigInteger delay;
        protected List<PassengerType> passenger;
        @XmlAttribute(name = "flightNumber", namespace = "http://dp2.polito.it/FDS/wsdl/FDSBasic", required = true)
        @XmlIDREF
        @XmlSchemaType(name = "IDREF")
        protected Object flightNumber;
        @XmlAttribute(name = "aircraftId", namespace = "http://dp2.polito.it/FDS/wsdl/FDSBasic")
        @XmlIDREF
        @XmlSchemaType(name = "IDREF")
        protected Object aircraftId;
        @XmlAttribute(name = "status", namespace = "http://dp2.polito.it/FDS/wsdl/FDSBasic")
        protected StatusEnumType status;

        /**
         * Gets the value of the deptDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDeptDate() {
            return deptDate;
        }

        /**
         * Sets the value of the deptDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDeptDate(XMLGregorianCalendar value) {
            this.deptDate = value;
        }

        /**
         * Gets the value of the gate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGate() {
            return gate;
        }

        /**
         * Sets the value of the gate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGate(String value) {
            this.gate = value;
        }

        /**
         * Gets the value of the delay property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDelay() {
            return delay;
        }

        /**
         * Sets the value of the delay property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDelay(BigInteger value) {
            this.delay = value;
        }

        /**
         * Gets the value of the passenger property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the passenger property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPassenger().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PassengerType }
         * 
         * 
         */
        public List<PassengerType> getPassenger() {
            if (passenger == null) {
                passenger = new ArrayList<PassengerType>();
            }
            return this.passenger;
        }

        /**
         * Gets the value of the flightNumber property.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getFlightNumber() {
            return flightNumber;
        }

        /**
         * Sets the value of the flightNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setFlightNumber(Object value) {
            this.flightNumber = value;
        }

        /**
         * Gets the value of the aircraftId property.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getAircraftId() {
            return aircraftId;
        }

        /**
         * Sets the value of the aircraftId property.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setAircraftId(Object value) {
            this.aircraftId = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link StatusEnumType }
         *     
         */
        public StatusEnumType getStatus() {
            if (status == null) {
                return StatusEnumType.BOOKING;
            } else {
                return status;
            }
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link StatusEnumType }
         *     
         */
        public void setStatus(StatusEnumType value) {
            this.status = value;
        }

    }

}
