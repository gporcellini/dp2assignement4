package it.polito.dp2.FDS.sol4.client;

public class FDSControlClientException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4287357556951147213L;

	private int _errorCode;
	
	public FDSControlClientException(int errorCode){
		_errorCode = errorCode;
	}
	
	public int getErrorCode() {
		return _errorCode;
	}
}
