package it.polito.dp2.FDS.sol4.client;


import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;
import it.polito.dp2.fds.client.jaxb.BoardList.BoardListType;
import it.polito.dp2.fds.client.jaxb.BoardList.ObjectFactory;
import it.polito.dp2.fds.client.jaxb.BoardList.PassengerType;
import it.polito.dp2.fds.client.jaxb.FDSBoarding.BoardingType;
import it.polito.dp2.fds.wsdl.fdsbasic.StatusEnumType;
import it.polito.dp2.fds.wsdl.fdscontrol.FDSControl;
import it.polito.dp2.fds.wsdl.fdscontrol.FDSControlService;
import it.polito.dp2.fds.wsdl.fdscontrol.GetBoardedPassengerListResponse.Passenger;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceStatusFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidPassengerNameFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidPassengerStatusFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidRequestFormatFault_Exception;
import it.polito.dp2.fds.wsdl.fdsinfo.FDSInfo;
import it.polito.dp2.fds.wsdl.fdsinfo.FDSInfoService;
import it.polito.dp2.fds.wsdl.fdsinfo.FlightInstanceNoPassengerListType;
import it.polito.dp2.fds.wsdl.fdsinfo.InvalidParametersFault_Exception;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.ws.Holder;

import org.xml.sax.SAXException;

public class FDSControlClient {
	private static final String FDSBOARDING_XSD_SCHEMA_PATH = "xsd/fdsBoarding.xsd";
	private static final String BOARDLIST_XSD_SCHEMA_PATH_STRING = "xsd/boardList.xsd";
	private static final String FDSINFO_URL_STRING = "http://localhost:7071/fdsinfo";
	private static final int DATA_ERROR = 1;
	private static final int SERVER_SIDE_FAILURE = 2;
	private static final int RESULTS_PER_PAGE = 50;
	private BoardingType _jaxbBoardingType;
	private FDSControl _controlProxy;
	private FDSInfo _infoProxy;
	private static final Logger LOGGER = Logger.getLogger(FDSControlClient.class.getName());
	
	/**
	 * @throws FDSControlClientDataErrorException 
	 * @category Constructor
	 */
	private FDSControlClient(String fileName) throws FDSControlClientException{
		if(fileName == null) System.exit(DATA_ERROR);		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance("it.polito.dp2.fds.client.jaxb.FDSBoarding");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File(FDSBOARDING_XSD_SCHEMA_PATH));
			unmarshaller.setSchema(schema);
			JAXBElement<BoardingType> jaxbElement = (JAXBElement<BoardingType>) unmarshaller.unmarshal(new File(fileName));
			_jaxbBoardingType = (BoardingType) jaxbElement.getValue();
		} catch (JAXBException | SAXException e) {
			LOGGER.info(e.getCause() + e.getMessage());
			throw new FDSControlClientException(DATA_ERROR);	
		}
		
		if(!ValidateFlightNumber(_jaxbBoardingType.getFlight())){
			LOGGER.info("Error: Invalid flight number");
			throw new FDSControlClientException(DATA_ERROR);	
		}
		
		URL serviceUrl;
		try {
			String endpointString = _jaxbBoardingType.getEndpoint();
			if(endpointString == null) System.exit(DATA_ERROR);
			serviceUrl = new URL(endpointString);
			FDSControlService service = new FDSControlService(serviceUrl);
			_controlProxy = service.getFDSControlServiceSOAPPort();
			
			serviceUrl = new URL(FDSINFO_URL_STRING);
			FDSInfoService fdsInfoService = new FDSInfoService(serviceUrl);
			_infoProxy = fdsInfoService.getFDSInfoServiceSOAPPort();
		} catch (MalformedURLException e) {
			LOGGER.info(e.getMessage());
			throw new FDSControlClientException(DATA_ERROR);	
		}
	}
	
	/**
	 * @category RPC
	 * @throws FDSControlClientDataErrorException
	 */
	private void startBoarding() throws FDSControlClientException {
		BoardingType.StartBoarding startBoarding = _jaxbBoardingType.getStartBoarding();
		
		 if(startBoarding != null){
			 try {
				_controlProxy.startBoarding(_jaxbBoardingType.getFlight(), _jaxbBoardingType.getDate());
			} catch (InvalidFlightInstanceFault_Exception e) {
				LOGGER.info(e.getMessage());
				throw new FDSControlClientException(DATA_ERROR);	
			} catch (InvalidFlightInstanceStatusFault_Exception e) {
				String expected = "";
				for(StatusEnumType statusEnumType : e.getFaultInfo().getExpectedStatus())
					expected += statusEnumType.name();
				LOGGER.info(_jaxbBoardingType.getFlight()+"@"+_jaxbBoardingType.getDate()+
						"Expected flightInstance status: " + expected + ", Actual status: " 
						+ e.getFaultInfo().getCurrentStatus());
				throw new FDSControlClientException(DATA_ERROR);	
			} catch (InvalidRequestFormatFault_Exception e) {
				LOGGER.info(_jaxbBoardingType.getFlight() + "@" + _jaxbBoardingType.getDate() +
						": invalid parameters in the request");
				throw new FDSControlClientException(DATA_ERROR);
			}
		 } 
	}

	/**
	 * 
	 * @throws FDSControlClientDataErrorException
	 */
	private void boardPassengers() throws FDSControlClientException {
		try {
			for (String passengerName : _jaxbBoardingType.getPassenger()) 
				_controlProxy.board(_jaxbBoardingType.getFlight(), _jaxbBoardingType.getDate(), passengerName);
		} catch (InvalidFlightInstanceFault_Exception e){
			LOGGER.info(_jaxbBoardingType.getFlight()+"@"+_jaxbBoardingType.getDate()+": no such flight instance");
			throw new FDSControlClientException(DATA_ERROR);
		} catch (InvalidFlightInstanceStatusFault_Exception e) {
			String expected = "";
			for(StatusEnumType statusEnumType : e.getFaultInfo().getExpectedStatus())
				expected += statusEnumType.name();
			LOGGER.info(_jaxbBoardingType.getFlight() + "@" + _jaxbBoardingType.getDate() + "Expected flightInstance status: "
					+ expected + ", Actual status: " + e.getFaultInfo().getCurrentStatus());
			throw new FDSControlClientException(DATA_ERROR);
		} catch (InvalidPassengerNameFault_Exception e) {
			LOGGER.info(_jaxbBoardingType.getFlight() + "@" + _jaxbBoardingType.getDate() +": no such passenger on this"
					+ " flight instance");
			throw new FDSControlClientException(DATA_ERROR);
		} catch (InvalidPassengerStatusFault_Exception e) {
			LOGGER.info(e.getMessage());
			throw new FDSControlClientException(DATA_ERROR);
		} catch (InvalidRequestFormatFault_Exception e) {
			LOGGER.info(_jaxbBoardingType.getFlight() + "@" + _jaxbBoardingType.getDate() +
					": invalid parameters in the request");
			throw new FDSControlClientException(DATA_ERROR);
		}
	}
	
	/**
	 * @param outputXMLFileName 
	 * @throws FDSControlClientDataErrorException 
	 * 
	 */
	private void printBoardedPassengersList(String outputXMLFileName) {
		try {
			if(outputXMLFileName == null){
				LOGGER.info("null output file name");
				throw new FDSControlClientException(DATA_ERROR);		
			}
			
			BoardListType boardList = new BoardListType();
			boardList.setDate(_jaxbBoardingType.getDate());
			boardList.setFlight(_jaxbBoardingType.getFlight());
			
			Holder<List<Passenger>> passengers = new Holder<List<Passenger>>();
			int pageNumber = 0;
			Holder<Boolean> morePages = new Holder<Boolean>(true);
			
			
			while(morePages.value){
				_controlProxy.getBoardedPassengerList(_jaxbBoardingType.getFlight(), _jaxbBoardingType.getDate(),
						pageNumber, RESULTS_PER_PAGE, passengers, morePages);
				pageNumber++;
				for (Passenger passenger : passengers.value) {
					PassengerType passengerType = new PassengerType();
					passengerType.setValue(passenger.getName());
					passengerType.setSeat(passenger.getSeat());
					boardList.getPassenger().add(passengerType);
				}
			}
			
			File file = new File(outputXMLFileName);
			
			JAXBContext jaxbContext = JAXBContext.newInstance("it.polito.dp2.fds.client.jaxb.BoardList");
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//reference schema in xml
			jaxbMarshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, BOARDLIST_XSD_SCHEMA_PATH_STRING);
			//validation schema
			SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File(BOARDLIST_XSD_SCHEMA_PATH_STRING));
            
            jaxbMarshaller.setSchema(schema);
            
			jaxbMarshaller.marshal(new ObjectFactory().createBoardList(boardList), file);
		} catch (InvalidFlightInstanceFault_Exception | JAXBException | SAXException e) {
			LOGGER.info(e.getCause() + e.getMessage());
			throw new FDSControlClientException(DATA_ERROR);	
		} catch (InvalidRequestFormatFault_Exception e) {
			LOGGER.info(_jaxbBoardingType.getFlight() + "@" + _jaxbBoardingType.getDate() +
					": invalid parameters in the request");
						throw new FDSControlClientException(DATA_ERROR);
		} catch (InvalidFlightInstanceStatusFault_Exception e) {
			String expected = "";
			for(StatusEnumType statusEnumType : e.getFaultInfo().getExpectedStatus())
				expected += statusEnumType.name();
			LOGGER.info(_jaxbBoardingType.getFlight() + "@" + _jaxbBoardingType.getDate() + "Expected flightInstance status: "
					+ expected + ", Actual status: " + e.getFaultInfo().getCurrentStatus());
			throw new FDSControlClientException(DATA_ERROR);
		}
	}
	
	
	//TEST METHODS
	
	@SuppressWarnings("unused")
	private void getAllFlightInstances(){
		try {
			Holder<List<FlightInstanceNoPassengerListType>> flightInstanceTypes =
					new Holder<List<FlightInstanceNoPassengerListType>>();
			int pageNumber = 0;
			Holder<Boolean> morePages = new Holder<Boolean>(true);
			while(morePages.value){
				_infoProxy.getFlightInstances("rferveqrcv", null, null, pageNumber, RESULTS_PER_PAGE, flightInstanceTypes, morePages);
				pageNumber++;
				for (FlightInstanceNoPassengerListType flightInstanceType : flightInstanceTypes.value) {
					System.out.println(flightInstanceType.getFlightNumber() + " " + flightInstanceType.getDeptDate().getDay() +"/"
							+ flightInstanceType.getDeptDate().getMonth()+"/"+flightInstanceType.getDeptDate().getYear());
				}
			}
		} catch (InvalidParametersFault_Exception e) {
			//  Auto-generated catch block
			LOGGER.info("InvalidParametersFault_Exception, "+e.getMessage());
		}
		
	}
	
	/**
	 * @category Validator
	 * @param flightNumber
	 * @return
	 */
	public static boolean ValidateFlightNumber(String flightNumber){
		if(flightNumber.matches("^[A-Za-z]{2}[0-9]{1,4}$")) return true;
		else return false;
	}

	
	/**
	 * 
	 * @param args
	 * @return
	 */
	public static void main(String[] args) {
		if(args.length != 2){
			//System.out.println("Error: usage inputFile.xml outputFile.xml");
			System.exit(DATA_ERROR);
		}
		
		FDSControlClient instance;
		try {
			instance = new FDSControlClient(args[0]);
			//instance.getAllFlightInstances();
			instance.startBoarding();
			instance.boardPassengers();
			instance.printBoardedPassengersList(args[1]);
		} catch (FDSControlClientException e){
			instance = null;
			System.gc();
			System.exit(e.getErrorCode());
		} catch (Exception e){
			LOGGER.info(e.getMessage());
			instance = null;
			System.gc();
			System.exit(SERVER_SIDE_FAILURE);
		}
	}

}
