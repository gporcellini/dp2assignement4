package it.polito.dp2.FDS.sol4.server;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightInstanceStatus;
import it.polito.dp2.FDS.FlightMonitor;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.Time;

public class Sol4FlightMonitor implements FlightMonitor{
	private SortedMap<String, Aircraft> _aircrafts;
	private SortedMap<String, SortedMap<GregorianCalendar, FlightInstanceReader>> _flightInstances; 
	private SortedMap<String, FlightReader> _flights;
	private static Sol4FlightMonitor _instance;
	
	/**
	 * @category Comparator
	 */
	Comparator<GregorianCalendar> customComparator = new Comparator<GregorianCalendar>() {

		@Override
		public int compare(GregorianCalendar o1, GregorianCalendar o2) {
			if(o1.get(Calendar.YEAR) == o2.get(Calendar.YEAR)){
				if(o1.get(Calendar.MONTH) == o2.get(Calendar.MONTH)){
					if(o1.get(Calendar.DAY_OF_MONTH) == o2.get(Calendar.DAY_OF_MONTH)){
						return 0;
					}else{
						return Integer.compare(o1.get(Calendar.DAY_OF_MONTH), o2.get(Calendar.DAY_OF_MONTH));
					}
				}else{
					return Integer.compare(o1.get(Calendar.MONTH), o2.get(Calendar.MONTH));
				}	
			}else{
				return Integer.compare(o1.get(Calendar.YEAR),o2.get(Calendar.YEAR));
			}
		}
	};
	
	//CONSTRUCTORS
	
	/**
	 * @category Static Getter(Singleton Pattern)
	 * @return
	 * @throws FlightMonitorException 
	 */
	public static Sol4FlightMonitor getInstance(FlightMonitor oldFlightMonitor) throws FlightMonitorException{
		if(_instance == null) _instance = new Sol4FlightMonitor(oldFlightMonitor);
		
		return _instance;
	}
    
	/**
	 * @throws FlightMonitorException 
	 * @category Constructor
	 */
	private Sol4FlightMonitor(FlightMonitor oldFlightMonitor) throws FlightMonitorException{
		_aircrafts = new TreeMap<String, Aircraft>();
		_flightInstances = new TreeMap<String, SortedMap<GregorianCalendar, FlightInstanceReader>>();
		_flights = new TreeMap<String, FlightReader>();
		Import(oldFlightMonitor);
	}
	
	
	//LOADERS
	
	/**
	 * @category Loader
	 * @param fileName
	 * @throws FlightMonitorException
	 */
	private void Import(FlightMonitor oldMonitor) throws FlightMonitorException{
		if(oldMonitor == null) throw new FlightMonitorException("Monitor to import was null");
		ImportAircrafts(oldMonitor.getAircrafts());
		try {
			ImportFlights(oldMonitor.getFlights(null, null, null));
			ImportFlightInstances(oldMonitor.getFlightInstances(null, null, null));
		} catch (MalformedArgumentException e) {
			// Never thrown because passing null parameters
		}
	}

	/**
	 * @category Loader
	 * @param jaxbFlightInstanceList
	 * @throws FlightMonitorException
	 */
	private void ImportFlightInstances(List<FlightInstanceReader> oldFlightInstanceReaderList) throws FlightMonitorException{		
		for(FlightInstanceReader oldFlightInstanceReader : oldFlightInstanceReaderList){
			FlightInstanceReader flightInstanceReader = new Sol4FlightInstanceReader(oldFlightInstanceReader);
			String flightNumber = flightInstanceReader.getFlight().getNumber();
			if(_flightInstances.containsKey(flightNumber)){
				_flightInstances.get(flightNumber).put(
						flightInstanceReader.getDate(), 
						flightInstanceReader);
			} else {
				SortedMap<GregorianCalendar, FlightInstanceReader> innerMap = 
						new TreeMap<GregorianCalendar, FlightInstanceReader>(customComparator);
				innerMap.put(flightInstanceReader.getDate(), flightInstanceReader);
				_flightInstances.put(flightNumber, innerMap);
			}
		}
		
	}

	/**
	 * @category Loader
	 * @param flightReaderList
	 * @throws FlightMonitorException
	 */
	private void ImportFlights(List<FlightReader> flightReaderList) throws FlightMonitorException{
		for(FlightReader flightReader : flightReaderList) {
			_flights.put(flightReader.getNumber(), flightReader);
		}
		
	}

	/**
	 * @category Loader
	 * @param oldAircraftSet
	 */
	private void ImportAircrafts(Set<Aircraft> oldAircraftSet) {
		for(Aircraft aircraft : oldAircraftSet){
			_aircrafts.put(aircraft.model, aircraft);
		}
		
	}

	
	//GETTERS
	
	/**
	 * @category Getter
	 */
	@Override
	public Set<Aircraft> getAircrafts() {
		return new HashSet<Aircraft>(_aircrafts.values());
	}
	
	/**
	 * @category Getter
	 */
	protected Aircraft getAircraft(String model) throws MalformedArgumentException{
		if(model == null) throw new MalformedArgumentException();
		
		if(_aircrafts.containsKey(model)) return _aircrafts.get(model);
		
		return null;
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightReader getFlight(String arg0)
			throws MalformedArgumentException { //tested
		if(arg0 == null || !ValidateFlightNumber(arg0)){
			throw new MalformedArgumentException();
		}
		
		if(_flights.containsKey(arg0)) return _flights.get(arg0);
		
		return null;
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightInstanceReader getFlightInstance(String arg0,
			GregorianCalendar arg1) throws MalformedArgumentException { //tested
		if(arg0 == null || arg1==null || !ValidateFlightNumber(arg0))
			throw new MalformedArgumentException();
		
		if(_flightInstances.containsKey(arg0)) {
			
			return _flightInstances.get(arg0).get(arg1);
		}
		
		return null;
	}

	/**
	 * @category Getter
	 */
	@Override
	public List<FlightInstanceReader> getFlightInstances(String arg0,
			GregorianCalendar arg1, FlightInstanceStatus arg2)
			throws MalformedArgumentException { //tested
		if(arg0 != null && !ValidateFlightNumber(arg0))
			throw new MalformedArgumentException();
		
		List<FlightInstanceReader> list = new ArrayList<FlightInstanceReader>();
		if(arg0 == null){
			for(SortedMap<GregorianCalendar, FlightInstanceReader> subMap : _flightInstances.values()){
				list.addAll(FlightInstanceSubList(subMap, arg1, arg2));
			}
		}else if(_flightInstances.containsKey(arg0)){
			SortedMap<GregorianCalendar, FlightInstanceReader> subMap = _flightInstances.get(arg0);
			list = FlightInstanceSubList(subMap, arg1, arg2);
		}
		
		return list;
	}
	
	/**
	 * @category Getter
	 */
	private List<FlightInstanceReader> FlightInstanceSubList(SortedMap<GregorianCalendar, FlightInstanceReader> subMap, 
			GregorianCalendar arg1,
			FlightInstanceStatus arg2){
		List<FlightInstanceReader> list = new ArrayList<FlightInstanceReader>();

		if(subMap.size() == 0) return list;
		if(arg1 != null) {
			if(customComparator.compare(arg1, subMap.lastKey()) > 0) return list;
			else subMap = subMap.tailMap(arg1);
		}
		
		for (FlightInstanceReader fir : subMap.values()){
			if(arg2 == null || fir.getStatus() == arg2){
				list.add(fir);
			}
		}
		
		return list;
	}

	/**
	 * @category Getter
	 */
	@Override
	public List<FlightReader> getFlights(String dep, String arr, Time time)
			throws MalformedArgumentException {
		if((dep != null && !ValidateIATACode(dep)) || 
				(arr != null && !ValidateIATACode(arr)) )
			throw new MalformedArgumentException();
		List<FlightReader> list = new ArrayList<FlightReader>();
		
		for(FlightReader flightReader : _flights.values())
			if(CheckFlight(flightReader, dep, arr, time)) list.add(flightReader);
		
		return list;
	
	}
	
	
	//VALIDATORS
	
	/**
	 * @category Validator	
	 * @param flightReader
	 * @param dep
	 * @param arr
	 * @param time
	 * @return
	 */
	private boolean CheckFlight(FlightReader flightReader, String dep, String arr, Time time){
		if(dep == null) return CheckFlightArr(flightReader, arr, time);
		else if(dep.equals(flightReader.getDepartureAirport()))	return CheckFlightArr(flightReader, arr, time);
		
		return false;
	}
	
	/**
	 * @category Validator
	 * @param flightReader
	 * @param arr
	 * @param time
	 * @return
	 */
	private boolean CheckFlightArr(FlightReader flightReader, String arr, Time time){
		if(arr == null) return CheckFlightTime(flightReader, time);
		else if(arr.equals(flightReader.getDestinationAirport())) return CheckFlightTime(flightReader, time);
		
		return false;
	}
	
	/**
	 * @category Validator
	 * @param flightReader
	 * @param time
	 * @return
	 */
	private boolean CheckFlightTime(FlightReader flightReader, Time time){
		if(time == null) return true;
		else if(!(flightReader.getDepartureTime()).precedes(time)) return true;
		
		return false;
	}
	
	/**
	 * @category Validator
	 * @param IATAcode
	 * @return
	 */
	public static boolean ValidateIATACode(String IATAcode){
		if(IATAcode.matches("^[A-Za-z]{3}$")) return true;
		else return false;
	}

	/**
	 * @category Validator
	 * @param flightNumber
	 * @return
	 */
	public static boolean ValidateFlightNumber(String flightNumber){
		if(flightNumber.matches("^[A-Za-z]{2}[0-9]{1,4}$")) return true;
		else return false;
	}

}
