package it.polito.dp2.FDS.sol4.server;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightInstanceStatus;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;
import it.polito.dp2.FDS.Time;
import it.polito.dp2.fds.wsdl.fdsinfo.FDSInfo;
import it.polito.dp2.fds.wsdl.fdsinfo.FlightInstanceNoPassengerListType;
import it.polito.dp2.fds.wsdl.fdsinfo.InvalidParametersFault;
import it.polito.dp2.fds.wsdl.fdsinfo.InvalidParametersFault_Exception;
import it.polito.dp2.fds.wsdl.fdsbasic.AircraftType;
import it.polito.dp2.fds.wsdl.fdsbasic.FlightType;
import it.polito.dp2.fds.wsdl.fdsbasic.PassengerType;
import it.polito.dp2.fds.wsdl.fdsbasic.StatusEnumType;
@HandlerChain(file="wsdl/info-handler-chain.xml")
@WebService(wsdlLocation="wsdl/FDSInfo.wsdl",
targetNamespace="http://dp2.polito.it/FDS/wsdl/FDSInfo",
serviceName="FDSInfoService",
portName="FDSInfoServiceSOAPPort",
endpointInterface="it.polito.dp2.fds.wsdl.fdsinfo.FDSInfo")
public class FDSInfoImpl implements FDSInfo{
	private Sol4FlightMonitor _flightMonitor;
	private static final Logger LOGGER = Logger.getLogger(FDSInfoImpl.class.getName());

	public FDSInfoImpl(Sol4FlightMonitor flightMonitor) {
		_flightMonitor = flightMonitor;
	}

	@Override
	public FlightType getFlight(String flightNumber)
			throws InvalidParametersFault_Exception {
		try {
			FlightReader flightReader = _flightMonitor.getFlight(flightNumber);
			if(flightReader == null) {
					String message = flightNumber + ": no such flight";
					LOGGER.info(message);
					throw new InvalidParametersFault_Exception(message, new InvalidParametersFault());
			}
			return toFlightType(flightReader);
		} catch (MalformedArgumentException e) {
			String message = "Invalid parameters (null or malformed)";
			LOGGER.info(message);
			throw new InvalidParametersFault_Exception(flightNumber, new InvalidParametersFault());
		}
	}

	@Override
	public void getFlights(String deptIATAcode, String destIATAcode,
			XMLGregorianCalendar deptTime, int pageNumber, int resultsPerPage,
			Holder<List<FlightType>> flight, Holder<Boolean> morePages)
			throws InvalidParametersFault_Exception {
		try {
			int startIndex = pageNumber * resultsPerPage ;
			/*
			if(startIndex < 0){
				throw new InvalidParametersFault_Exception("pageNumber*resultsPerPage must be >= 0", new InvalidParametersFault());
			} not necessary as validated*/
			
			List<FlightReader> flightReaders = _flightMonitor.getFlights(deptIATAcode,
					destIATAcode, 
					deptTime == null ? null : new Time(deptTime.getHour(), deptTime.getMinute()));
			
			flight.value = new ArrayList<FlightType>();
			if(startIndex < flightReaders.size()){//start index in boundaries
				int endIndex = startIndex + resultsPerPage;
				if(endIndex >= flightReaders.size()){//end index in boundaries
					morePages.value = false;
					endIndex=flightReaders.size();
				} else {
					morePages.value = true;
				}
				for (FlightReader flightReader : flightReaders.subList(startIndex, endIndex))
					flight.value.add(toFlightType(flightReader));
			} else {
				morePages.value = false;
			}
		} catch (IllegalArgumentException | MalformedArgumentException e) {
			throw new InvalidParametersFault_Exception("Invalid Parameters", new InvalidParametersFault());
		}
	}
	
	@Override
	public FlightInstanceNoPassengerListType getFlightInstance(String flightNumber,
			XMLGregorianCalendar deptDate)
			throws InvalidParametersFault_Exception {
		return toFlightInstanceNoPassengerListType(
					innerGetFlightInstance(flightNumber, deptDate));
	}

	@Override
	public void getFlightInstances(String flightNumber,
			XMLGregorianCalendar deptDate, StatusEnumType status,
			int pageNumber, int resultsPerPage,
			Holder<List<FlightInstanceNoPassengerListType>> flightInstance,
			Holder<Boolean> morePages) throws InvalidParametersFault_Exception {
		try {
			int startIndex = pageNumber * resultsPerPage ;
			/*
			if(startIndex < 0){
				throw new InvalidParametersFault_Exception("pageNumber*resultsPerPage must be >= 0", new InvalidParametersFault());
			} not necessary as validated*/
			
			FlightInstanceStatus flightInstanceStatus = status == null ? null: FlightInstanceStatus.valueOf(status.name());
			List<FlightInstanceReader> flightInstanceReaders =
					_flightMonitor.getFlightInstances(flightNumber,
							deptDate == null ? null : deptDate.toGregorianCalendar(), 
							flightInstanceStatus);
			
			flightInstance.value = new ArrayList<FlightInstanceNoPassengerListType>();
			if(startIndex < flightInstanceReaders.size()){//start index in boundaries
				int endIndex = startIndex + resultsPerPage;
				if(endIndex >= flightInstanceReaders.size()){//end index in boundaries
					morePages.value = false;
					endIndex=flightInstanceReaders.size();
				} else {
					morePages.value = true;
				}
				for (FlightInstanceReader flightInstanceReader : flightInstanceReaders.subList(startIndex, endIndex)){
					synchronized (flightInstanceReader) {
						if(flightInstanceStatus == null ||
								flightInstanceReader.getStatus() == flightInstanceStatus)
							flightInstance.value.add(toFlightInstanceNoPassengerListType(flightInstanceReader));
					}
				}
			} else {
				morePages.value = false;
			}
		} catch (MalformedArgumentException e) {
			throw new InvalidParametersFault_Exception("Invalid Parameters", new InvalidParametersFault());
		}
	}
	
	/**
	 * @category Converter
	 * @param flightReader
	 * @return
	 * @throws InvalidParametersFault_Exception
	 */
	private FlightType toFlightType(FlightReader flightReader) throws InvalidParametersFault_Exception{
		if(flightReader == null) return null;
		FlightType flightType = new FlightType();
		flightType.setNumber(flightReader.getNumber());
		flightType.setDeptIATAcode(flightReader.getDepartureAirport());
		flightType.setDestIATAcode(flightReader.getDestinationAirport());
		try {
			XMLGregorianCalendar tmpDeptTime = DatatypeFactory.newInstance().newXMLGregorianCalendar();
			tmpDeptTime.clear();
			tmpDeptTime.setHour(flightReader.getDepartureTime().getHour());
			tmpDeptTime.setMinute(flightReader.getDepartureTime().getMinute());
			tmpDeptTime.setSecond(0);
			flightType.setDeptTime(tmpDeptTime);
		} catch (DatatypeConfigurationException e) {
			 /*this execption should never be raised as input source should be reliable*/ 
			throw new InvalidParametersFault_Exception("Error in server database", new InvalidParametersFault());
		};
				
		return flightType;
	}
	
	/**
	 * @category Converter
	 * @param flightInstanceReader
	 * @return
	 * @throws InvalidParametersFault_Exception
	 */
	private FlightInstanceNoPassengerListType toFlightInstanceNoPassengerListType(FlightInstanceReader flightInstanceReader) throws InvalidParametersFault_Exception{
		if(flightInstanceReader == null) return null;
		FlightInstanceNoPassengerListType flightInstanceType = new FlightInstanceNoPassengerListType();
		flightInstanceType.setAircraftId(flightInstanceReader.getAircraft().model);
		flightInstanceType.setFlightNumber(flightInstanceReader.getFlight().getNumber());
		flightInstanceType.setDelay(BigInteger.valueOf(flightInstanceReader.getDelay()));
		flightInstanceType.setGate(flightInstanceReader.getDepartureGate());
		try {
			flightInstanceType.setDeptDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(flightInstanceReader.getDate()));
		} catch (DatatypeConfigurationException e) {
			/*should never happen as my db should be reliable*/
			throw new InvalidParametersFault_Exception("Error in server database", new InvalidParametersFault());
		}
		flightInstanceType.setStatus(StatusEnumType.valueOf(flightInstanceReader.getStatus().name()));
		
		return flightInstanceType;
	}
	
	/**
	 * @category Converter
	 * @param aircraft
	 * @return
	 */
	@SuppressWarnings("unused")
	private AircraftType toAiracraftType(Aircraft aircraft){
		if(aircraft == null) return null;
		AircraftType aircraftType = new AircraftType();
		aircraftType.setModel(aircraft.model);
		for (String seat : aircraft.seats) 
			aircraftType.getSeat().add(seat);
		
		return aircraftType;
	}
	
	/**
	 * @category Converter
	 * @param passengerReader
	 * @return
	 */
	private PassengerType toPassengerType(PassengerReader passengerReader){
		if(passengerReader == null) return null;
		PassengerType passengerType = new PassengerType();
		passengerType.setName(passengerReader.getName());
		passengerType.setSeat(passengerReader.getSeat());
		passengerType.setBoarded(passengerReader.boarded());
		
		return passengerType;
	}

	@Override
	public void getPassengerList(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate, String namePrefix,
			int pageNumber, int resultsPerPage,
			Holder<List<PassengerType>> passenger, Holder<Boolean> morePages)
			throws InvalidParametersFault_Exception {
		int startIndex = pageNumber * resultsPerPage ;
		/*
		if(startIndex < 0){
			throw new InvalidParametersFault_Exception("pageNumber*resultsPerPage must be >= 0", new InvalidParametersFault());
		} not necessary as validated*/
		
		Set<PassengerReader> passengerReaders = innerGetFlightInstance(flightInstanceFlightNumber,
				flightInstanceDeptDate).getPassengerReaders(namePrefix);
		LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": requested passengers list");
		if(startIndex < passengerReaders.size()){//start index in boundaries
			int endIndex = startIndex + resultsPerPage;
			if(endIndex >= passengerReaders.size()){//end index in boundaries
				morePages.value = false;
				endIndex=passengerReaders.size();
			} else {
				morePages.value = true;
			}
			passenger.value = new ArrayList<PassengerType>();
			ArrayList<PassengerReader> passengerReaderList = new ArrayList<PassengerReader>(passengerReaders);
			for (PassengerReader passengerReader : passengerReaderList.subList(startIndex, endIndex)) {
				passenger.value.add(toPassengerType(passengerReader));
			}
		}else{
				morePages.value = false;
		}
	}
	
	private Sol4FlightInstanceReader innerGetFlightInstance(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate) throws InvalidParametersFault_Exception{

		/*if(flightInstanceFlightNumber == null | flightInstanceDeptDate == null){
			String message = "getFlightInstance: null arguments in input";
			LOGGER.info(message);
			throw new InvalidParametersFault_Exception(message, new InvalidParametersFault());
		} not necessary as validated*/
		
		Sol4FlightInstanceReader flightInstance = null;
		try {
			flightInstance=(Sol4FlightInstanceReader) _flightMonitor.getFlightInstance(
					flightInstanceFlightNumber, flightInstanceDeptDate.toGregorianCalendar());
		} catch (MalformedArgumentException e) {
			String message = "getFlightInstance: invalid arguments (malformed)";
			LOGGER.info(message);
			throw new InvalidParametersFault_Exception(message, new InvalidParametersFault());
		}
		
		if(flightInstance == null){
			String message = flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": no such flight instance";
			LOGGER.info(message);
			throw new InvalidParametersFault_Exception(message, new InvalidParametersFault());
		}
		
		return flightInstance;
	}
	
	@Override
	public void getAircrafts(int pageNumber, int resultsPerPage,
			Holder<List<AircraftType>> aircraft, Holder<Boolean> morePages) {
		// TODO Auto-generated method stub
		
	}

}
