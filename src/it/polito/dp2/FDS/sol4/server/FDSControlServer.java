package it.polito.dp2.FDS.sol4.server;

import java.io.FileNotFoundException;
import java.util.concurrent.Executors;

import javax.xml.ws.Endpoint;
import javax.xml.ws.http.HTTPBinding;

import it.polito.dp2.FDS.FlightMonitor;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightMonitorFactory;

public class FDSControlServer {
	private static final int THREAD_POOL_SIZE = 15;
	protected static final String CONTROL_SERVICE_URL = "http://localhost:7070/fdscontrol";
	protected static final String CONTROL_TYPES_XSD_URL = "http://localhost:7070/FDSControlServiceTypes.xsd";
	protected static final String INFO_TYPES_XSD_URL = "http://localhost:7071/FDSInfoServiceTypes.xsd";
	protected static final String BASIC_XSD_URL = "http://localhost:7070/FDSBasicTypes.xsd";
	protected static final String BASIC_XSD_URL_1 = "http://localhost:7071/FDSBasicTypes.xsd";
	protected static final String INFO_SERVICE_URL = "http://localhost:7071/fdsinfo";
	protected static final String CONTROL_CHECKIN_SERVICE_URL = "http://localhost:7070/fdscontrolcheckin";
	protected static final String CONTROL_OTHER_SERVICE_URL = "http://localhost:7070/fdscontrolother";

//this is the publisher
	public static void main(String args[]){
		try {
			//Initialize the data
			System.setProperty("it.polito.dp2.FDS.FlightMonitorFactory", "it.polito.dp2.FDS.Random.FlightMonitorFactoryImpl");
			FlightMonitorFactory factory = FlightMonitorFactory.newInstance();
			FlightMonitor oldFlightMonitor = factory.newFlightMonitor();
			if(oldFlightMonitor == null){
				System.out.println("Error input FlightMonitor was null");
				return;
			}
			Sol4FlightMonitor flightMonitor = Sol4FlightMonitor.getInstance(oldFlightMonitor);
			
			if(flightMonitor == null){
				System.out.println("Error instantiating new FlightMonitor");
				return;
			}
			
			//Publish FDSControlServiceTypes.xsd
			Endpoint e;
			try {
				e = Endpoint.create( HTTPBinding.HTTP_BINDING,
			        	 new XmlFileProvider("wsdl/FDSBasicTypes.xsd"));
				e.publish(BASIC_XSD_URL);
				e = Endpoint.create( HTTPBinding.HTTP_BINDING,
			        	 new XmlFileProvider("wsdl/FDSBasicTypes.xsd"));
				e.publish(BASIC_XSD_URL_1);
				e = Endpoint.create( HTTPBinding.HTTP_BINDING,
				        	 new XmlFileProvider("wsdl/FDSControlServiceTypes.xsd"));
				e.publish(CONTROL_TYPES_XSD_URL);
				
				e = Endpoint.create( HTTPBinding.HTTP_BINDING,
			        	 new XmlFileProvider("wsdl/FDSInfoServiceTypes.xsd"));
				e.publish(INFO_TYPES_XSD_URL);
			} catch (FileNotFoundException e1) {
				System.err.println("Unable to open xsd file");
				e1.printStackTrace();
			}
			//Publish FDSControl
			publishService(new FDSControlImpl(flightMonitor), CONTROL_SERVICE_URL);
			//Publish FDSControlCheckIn
			publishService(new FDSControlCheckInImpl(), CONTROL_CHECKIN_SERVICE_URL);
			//Publish FDSControlOther
			publishService(new FDSControlOtherImpl(), CONTROL_OTHER_SERVICE_URL);
			//Publish FDSInfo
			publishService(new FDSInfoImpl(flightMonitor),INFO_SERVICE_URL);
		} catch (FlightMonitorException e) {
			// Never happens as input should be reliable
			e.printStackTrace();
		}
	}
	
	private static void publishService(Object toPublish, String url){
		Endpoint controlEndpoint = Endpoint.create(toPublish);
		controlEndpoint.setExecutor(Executors.newScheduledThreadPool(THREAD_POOL_SIZE));
		controlEndpoint.publish(url);
	}
}
