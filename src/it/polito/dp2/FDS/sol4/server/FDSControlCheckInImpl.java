package it.polito.dp2.FDS.sol4.server;

import java.math.BigInteger;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.fds.wsdl.fdscontrol.FDSControlCheckIn;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceStatusFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidPassengerNameFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidRequestFormatFault_Exception;

@HandlerChain(file="wsdl/handler-chain.xml")
@WebService(wsdlLocation="wsdl/FDSControl.wsdl",
targetNamespace="http://dp2.polito.it/FDS/wsdl/FDSControl",
serviceName="FDSControlCheckInService",
portName="FDSControlCheckInServiceSOAPPort",
endpointInterface="it.polito.dp2.fds.wsdl.fdscontrol.FDSControlCheckIn")
public class FDSControlCheckInImpl implements FDSControlCheckIn {

	@Override
	public void checkIn(String flightNumber, XMLGregorianCalendar deptDate,
			String passengerName, Holder<String> seat, Holder<String> gate,
			Holder<BigInteger> delay)
			throws InvalidFlightInstanceFault_Exception,
			InvalidFlightInstanceStatusFault_Exception,
			InvalidPassengerNameFault_Exception,
			InvalidRequestFormatFault_Exception {
		// TODO Auto-generated method stub
		
	}

}
