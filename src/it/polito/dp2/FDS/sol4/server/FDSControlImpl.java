package it.polito.dp2.FDS.sol4.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightInstanceStatus;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;
import it.polito.dp2.fds.wsdl.fdscontrol.FDSControl;
import it.polito.dp2.fds.wsdl.fdscontrol.GetBoardedPassengerListResponse.Passenger;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceFault;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceStatusFault;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceStatusFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidPassengerNameFault;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidPassengerNameFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidPassengerStatusFault;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidPassengerStatusFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidRequestFormatFault_Exception;
import it.polito.dp2.fds.wsdl.fdsbasic.StatusEnumType;

@HandlerChain(file="wsdl/handler-chain.xml")
@WebService(wsdlLocation="wsdl/FDSControl.wsdl",
targetNamespace="http://dp2.polito.it/FDS/wsdl/FDSControl",
serviceName="FDSControlService",
portName="FDSControlServiceSOAPPort",
endpointInterface="it.polito.dp2.fds.wsdl.fdscontrol.FDSControl")
public class FDSControlImpl implements FDSControl{
	Sol4FlightMonitor _flightMonitor;
	private static final Logger LOGGER = Logger.getLogger(FDSControlImpl.class.getName());
	
	/**
	 * @category Constructor
	 * @param flightMonitor
	 */
	public FDSControlImpl(Sol4FlightMonitor flightMonitor) {
		_flightMonitor = flightMonitor;
	}
	
	/**
	 * @category SOAPServiceOperation
	 */
	@Override
	public void board(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate, String passengerName)
			throws InvalidFlightInstanceFault_Exception,
			InvalidPassengerNameFault_Exception,
			InvalidFlightInstanceStatusFault_Exception,
			InvalidRequestFormatFault_Exception,
			InvalidPassengerStatusFault_Exception{
		
		if(passengerName == null){
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": Passenger name was null");
			throw new InvalidPassengerNameFault_Exception("Passenger name was null", new InvalidPassengerNameFault());
		}
		
		LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": Requested boarding operation for passenger " 
				+ passengerName);
		Sol4FlightInstanceReader flightInstance = getFlightInstance(flightInstanceFlightNumber, flightInstanceDeptDate);
		synchronized (flightInstance) {
			if(flightInstance.getStatus() != FlightInstanceStatus.BOARDING) {
				InvalidFlightInstanceStatusFault fault = new InvalidFlightInstanceStatusFault();
				fault.getExpectedStatus().add(StatusEnumType.BOARDING);
				fault.setCurrentStatus(StatusEnumType.valueOf(flightInstance.getStatus().name()));
				LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": Invalid status for boarding operation");
				throw new InvalidFlightInstanceStatusFault_Exception(flightInstanceFlightNumber + flightInstanceDeptDate, fault);
			}
				
			Sol4PassengerReader passengerReader;

			try {
				passengerReader = (Sol4PassengerReader) flightInstance.getPassengerReader(passengerName);
			} catch (MalformedArgumentException e) {
				//Never happens as already checked passengerName!=null
				throw new InvalidPassengerNameFault_Exception(passengerName, new InvalidPassengerNameFault());
			}
			
			if(passengerReader == null){
				LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": " 
						+ "passenger " + passengerName + "not registered on this flightInstance");
				throw new InvalidPassengerNameFault_Exception(passengerName, new InvalidPassengerNameFault());
			}
			if(passengerReader.getSeat() == null){
				String message = flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": passenger "
						+ passengerName + "couldn't be boarded as was not checked in";
				LOGGER.info(message);
				throw new InvalidPassengerStatusFault_Exception(message, new InvalidPassengerStatusFault());
			}
			
			if(!passengerReader.setBoarded(true)){
				String message = flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": passenger "
						+ passengerName + " already boarded";
				LOGGER.info(message);
				throw new InvalidPassengerStatusFault_Exception(message, new InvalidPassengerStatusFault());
			}
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": passenger "
						+ passengerName + " boarded");
		}
	}
	
	/**
	 * @category SOAPServiceOperation
	 */
	@Override
	public void startBoarding(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate)
			throws InvalidFlightInstanceFault_Exception,
			InvalidFlightInstanceStatusFault_Exception,
			InvalidRequestFormatFault_Exception {
		LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": requested boarding operation");
		Sol4FlightInstanceReader flightInstanceReader = getFlightInstance(flightInstanceFlightNumber, flightInstanceDeptDate);
		synchronized (flightInstanceReader) {
			/*if(flightInstanceReader.getStatus() == FlightInstanceStatus.BOARDING){
				LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": already boarding");
			} else */if(flightInstanceReader.getStatus() == FlightInstanceStatus.CHECKINGIN) {
				flightInstanceReader.setFlightInstanceStatus(FlightInstanceStatus.BOARDING);
				LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": now boarding");
			} else {
				InvalidFlightInstanceStatusFault fault = new InvalidFlightInstanceStatusFault();
				fault.getExpectedStatus().add(StatusEnumType.CHECKINGIN);
				//fault.getExpectedStatus().add(StatusEnumType.BOARDING);
				fault.setCurrentStatus(StatusEnumType.valueOf(flightInstanceReader.getStatus().name()));
				LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ":" +
						"status of flight instance cannot pass from " + flightInstanceReader.getStatus()
						+ " to BOARDING");
				throw new InvalidFlightInstanceStatusFault_Exception(flightInstanceFlightNumber + "@" + flightInstanceDeptDate, fault);
			}
		}
	}
	
	/**
	 * @category Getter
	 * @param flightInstanceFlightNumber
	 * @param flightInstanceDeptDate
	 * @return  valid flightInstance
	 * @throws InvalidFlightInstanceFault_Exception if invalid parameters or no such flightInstance
	 */
	private Sol4FlightInstanceReader getFlightInstance(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate) throws InvalidFlightInstanceFault_Exception {
		
		/*if(flightInstanceFlightNumber == null | flightInstanceDeptDate == null){
			LOGGER.info("getFlightInstance: null arguments in input");
			throw new InvalidFlightInstanceFault_Exception(
					"null parameters", new InvalidFlightInstanceFault());
		} not necessary as validated*/
		Sol4FlightInstanceReader flightInstance = null;
		try {
			flightInstance=(Sol4FlightInstanceReader) _flightMonitor.getFlightInstance(
					flightInstanceFlightNumber, flightInstanceDeptDate.toGregorianCalendar());
		} catch (MalformedArgumentException e) {
			LOGGER.info("getFlightInstance: invalid arguments (null or malformed)");
			throw new InvalidFlightInstanceFault_Exception(
					"getFlightInstance: invalid arguments (null or malformed)", new InvalidFlightInstanceFault());
		}
		
		if(flightInstance == null){
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": no such flight instance");
			throw new InvalidFlightInstanceFault_Exception(
					flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": no such flight instance", 
					new InvalidFlightInstanceFault());
		}
		return flightInstance;
	}
	
	/**
	 * @throws InvalidFlightInstanceStatusFault_Exception 
	 * @category SOAPServiceOperation
	 */
	@Override
	public void getBoardedPassengerList(String flightNumber,
			XMLGregorianCalendar deptDate, int pageNumber, int resultsPerPage,
			Holder<List<Passenger>> passenger, Holder<Boolean> morePages)
			throws InvalidFlightInstanceFault_Exception,
			InvalidRequestFormatFault_Exception, InvalidFlightInstanceStatusFault_Exception   {
		int startIndex = pageNumber * resultsPerPage ;
		/*
		if(startIndex < 0){
			throw new InvalidRequestFormatFault_Exception("pageNumber*resultsPerPage must be >= 0",
					new InvalidRequestFormatFault());
		} not necessary as validated*/
		
		FlightInstanceReader flightInstance =  getFlightInstance(flightNumber,
				deptDate);
		if(flightInstance.getStatus() != FlightInstanceStatus.BOARDING &&
				flightInstance.getStatus() != FlightInstanceStatus.ARRIVED &&
				flightInstance.getStatus() != FlightInstanceStatus.DEPARTED) {
			InvalidFlightInstanceStatusFault fault = new InvalidFlightInstanceStatusFault();
			fault.getExpectedStatus().add(StatusEnumType.BOARDING);
			fault.getExpectedStatus().add(StatusEnumType.ARRIVED);
			fault.getExpectedStatus().add(StatusEnumType.DEPARTED);
			fault.setCurrentStatus(StatusEnumType.valueOf(flightInstance.getStatus().name()));
			LOGGER.info(flightNumber + "@" + deptDate + ": Invalid status for boarding operation");
			throw new InvalidFlightInstanceStatusFault_Exception(flightNumber + deptDate, fault);
		}
		Set<PassengerReader> passengerReaderList = flightInstance.getPassengerReaders(null);
		ArrayList<PassengerReader> boardedPassengerReaderList = new ArrayList<PassengerReader>();
		for (PassengerReader passengerReader : passengerReaderList) {
			if(passengerReader.boarded()) boardedPassengerReaderList.add(passengerReader);
		}
		LOGGER.info(flightNumber + "@" + deptDate + ": requested boarded passengers list");
		if(startIndex < boardedPassengerReaderList.size()){//start index in boundaries
			int endIndex = startIndex + resultsPerPage;
			if(endIndex >= boardedPassengerReaderList.size()){//end index in boundaries
				morePages.value = false;
				endIndex=boardedPassengerReaderList.size();
			} else {
				morePages.value = true;
			}
			passenger.value = new ArrayList<Passenger>();
			
			for (PassengerReader passengerReader : boardedPassengerReaderList.subList(startIndex, endIndex)) {
				Passenger p = new Passenger();
				p.setName(passengerReader.getName());
				p.setSeat(passengerReader.getSeat());
				passenger.value.add(p);
			}
		}else{
				morePages.value = false;
		}
	}
}
