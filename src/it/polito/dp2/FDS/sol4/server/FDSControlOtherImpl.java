package it.polito.dp2.FDS.sol4.server;

import java.math.BigInteger;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;

import it.polito.dp2.fds.wsdl.fdscontrol.FDSControlOther;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidRequestFormatFault_Exception;

@HandlerChain(file="wsdl/handler-chain.xml")
@WebService(wsdlLocation="wsdl/FDSControl.wsdl",
targetNamespace="http://dp2.polito.it/FDS/wsdl/FDSControl",
serviceName="FDSControlOtherService",
portName="FDSControlOtherServiceSOAPPort",
endpointInterface="it.polito.dp2.fds.wsdl.fdscontrol.FDSControlOther")
public class FDSControlOtherImpl implements FDSControlOther{

	@Override
	public void setFlightInstanceGate(String flightNumber,
			XMLGregorianCalendar deptDate, String gate)
			throws InvalidFlightInstanceFault_Exception,
			InvalidRequestFormatFault_Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFlightInstanceDelay(String flightNumber,
			XMLGregorianCalendar deptDate, BigInteger delay)
			throws InvalidFlightInstanceFault_Exception,
			InvalidRequestFormatFault_Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancelFlightInstance(String flightNumber,
			XMLGregorianCalendar deptDate)
			throws InvalidFlightInstanceFault_Exception,
			InvalidRequestFormatFault_Exception {
		// TODO Auto-generated method stub
		
	}

}
