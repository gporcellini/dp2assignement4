package it.polito.dp2.FDS.sol4.server;

public class FDSInfoWsdlValidationHandler extends FDSControlWsdlValidationHandler {
	public FDSInfoWsdlValidationHandler() {
		schemaLocation = FDSControlServer.INFO_TYPES_XSD_URL;
		jaxbPackage = "it.polito.dp2.fds.wsdl.fdsinfo";
		_faultName = "InvalidParametersFault";
		_ns3 = "http://dp2.polito.it/FDS/wsdl/FDSInfo";
	}

}
