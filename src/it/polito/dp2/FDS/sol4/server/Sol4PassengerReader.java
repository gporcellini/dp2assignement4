package it.polito.dp2.FDS.sol4.server;

import java.util.concurrent.atomic.AtomicBoolean;

import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.PassengerReader;

public class Sol4PassengerReader implements PassengerReader, Comparable<Sol4PassengerReader>{
	private String _name;
	private String _seat;
	private AtomicBoolean _boarded;
	private FlightInstanceReader _flightInstanceReader;
	
	/**
	 * @category Constructor
	 * @param jaxbPassenger
	 * @param flightInstanceReader
	 */
	public Sol4PassengerReader(PassengerReader oldPassengerReader) {
		_name = oldPassengerReader.getName();
		_seat = oldPassengerReader.getSeat();	
		_boarded = new AtomicBoolean(oldPassengerReader.boarded());
		_flightInstanceReader = oldPassengerReader.getFlightInstance();
	}
	
	/**
	 * @category Setter
	 * @param value
	 */
	
	protected boolean setBoarded(boolean value){
		return _boarded.compareAndSet(!value, value);
	}

	/**
	 * @category Getter
	 */
	@Override
	public boolean boarded() {
		return _boarded.get();
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightInstanceReader getFlightInstance() {
		return _flightInstanceReader;
	}

	/**
	 * @category Getter
	 */
	@Override
	public String getName() {
		return _name;
	}

	/**
	 * @category Getter
	 */
	@Override
	public String getSeat() {
		return _seat;
	}

	@Override
	public int compareTo(Sol4PassengerReader o) {
		return _name.compareTo(o._name);
	}

}
