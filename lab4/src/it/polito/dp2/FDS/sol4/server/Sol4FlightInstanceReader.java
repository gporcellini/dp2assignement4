package it.polito.dp2.FDS.sol4.server;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightInstanceStatus;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;

public class Sol4FlightInstanceReader implements FlightInstanceReader{
	private Aircraft _aircraft;
	private FlightReader _flightReader;
	private GregorianCalendar _deptDate;
	private int _delay;
	private String _gate;
	private Map<String, Sol4PassengerReader> _passengers;
	private FlightInstanceStatus _status;
	
	/**
	 * @category Constructor
	 * @param jaxbFlightInstance
	 * @param caller
	 * @throws FlightMonitorException
	 */
	public Sol4FlightInstanceReader(FlightInstanceReader oldFlightInstanceReader) throws FlightMonitorException {
		// NB names of this getters are misleading as they actually return an object reference
		_aircraft = oldFlightInstanceReader.getAircraft();
		_flightReader = oldFlightInstanceReader.getFlight();
		_delay = oldFlightInstanceReader.getDelay();
		_gate = oldFlightInstanceReader.getDepartureGate();
		_status = oldFlightInstanceReader.getStatus();
		_deptDate = oldFlightInstanceReader.getDate();
		_passengers = new HashMap<String, Sol4PassengerReader>();
		for(PassengerReader oldPassenger : oldFlightInstanceReader.getPassengerReaders(null)){
			_passengers.put(oldPassenger.getName(), new Sol4PassengerReader(oldPassenger));
		}
	}
	
	protected void setPassengerBoarded(String passengerName, boolean value) throws MalformedArgumentException{
		if(!_passengers.containsKey(passengerName))
			throw new MalformedArgumentException("No such passenger on flightInstance");
		
		_passengers.get(passengerName).setBoarded(value);
	}
	
	/**
	 * @category Setter
	 * @param status
	 */
	protected void setFlightInstanceStatus(FlightInstanceStatus status){
		_status = status;
	}
	
	/**
	 * @category Getter
	 */
	@Override
	public Aircraft getAircraft() {
		return _aircraft;
	}

	/**
	 * @category Getter
	 */
	@Override
	public GregorianCalendar getDate() {
		return _deptDate;
	}

	/**
	 * @category Getter
	 */
	@Override
	public int getDelay() {
		return _delay;
	}

	/**
	 * @category Getter
	 */
	@Override
	public String getDepartureGate() {
		return _gate;
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightReader getFlight() {
		return _flightReader;
	}
	
	/**
	 * @category Getter
	 * @param passengerName
	 * @return null if no such passenger on flightInstance
	 * @throws MalformedArgumentException
	 */
	
	protected PassengerReader getPassengerReader(String passengerName) throws MalformedArgumentException{
		if(passengerName == null) throw new MalformedArgumentException();
		if(_passengers.containsKey(passengerName)) return _passengers.get(passengerName);
		else return null;
	}

	/**
	 * @category Getter
	 */
	@Override
	public Set<PassengerReader> getPassengerReaders(String arg0) {//tested
		if(arg0 == null){
			return new HashSet<PassengerReader>(_passengers.values());
		}
		Set<PassengerReader> subSet = new HashSet<PassengerReader>();
		for(String passengerName : _passengers.keySet()){
			if(passengerName.toLowerCase().startsWith(arg0.toLowerCase()))
				subSet.add(_passengers.get(passengerName));
		}
		return subSet;
	}

	/**
	 * @category Getter
	 */
	@Override
	public FlightInstanceStatus getStatus() {
		return _status;
	}
}
