package it.polito.dp2.FDS.sol4.server;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.FDS.FlightInstanceStatus;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;
import it.polito.dp2.fds.wsdl.fdscontrol.FDSControl;
import it.polito.dp2.fds.wsdl.fdscontrol.GetBoardedPassengerListResponse.Passenger;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceFault;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceStatusFault;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidFlightInstanceStatusFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidPassengerNameFault;
import it.polito.dp2.fds.wsdl.fdscontrol.InvalidPassengerNameFault_Exception;
import it.polito.dp2.fds.wsdl.fdscontrol.PassengerNotCheckedInFault;
import it.polito.dp2.fds.wsdl.fdscontrol.PassengerNotCheckedInFault_Exception;
import it.polito.dp2.fds.xsd.fdsinfo.StatusEnumType;

@WebService(wsdlLocation="wsdl/FDSControl.wsdl",
targetNamespace="http://dp2.polito.it/FDS/wsdl/FDSControl",
serviceName="FDSControlService",
portName="FDSControlServiceSOAPPort",
endpointInterface="it.polito.dp2.fds.wsdl.fdscontrol.FDSControl")
public class FDSControlImpl implements FDSControl{
	Sol4FlightMonitor _flightMonitor;
	private static final Logger LOGGER = Logger.getLogger(FDSControlImpl.class.getName());
	
	/**
	 * @category Constructor
	 * @param flightMonitor
	 */
	public FDSControlImpl(Sol4FlightMonitor flightMonitor) {
		_flightMonitor = flightMonitor;
	}
	
	/**
	 * @category SOAPServiceOperation
	 */
	@Override
	public void board(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate, String passengerName)
			throws InvalidFlightInstanceFault_Exception,
			InvalidPassengerNameFault_Exception,
			PassengerNotCheckedInFault_Exception,
			InvalidFlightInstanceStatusFault_Exception{
		
		if(passengerName == null){
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": Passenger name was null");
			throw new InvalidPassengerNameFault_Exception("Passenger name was null", new InvalidPassengerNameFault());
		}
		
		LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": Requested boarding operation for passenger " 
				+ passengerName);
		Sol4FlightInstanceReader flightInstance = getFlightInstance(flightInstanceFlightNumber, flightInstanceDeptDate);
		if(flightInstance.getStatus() != FlightInstanceStatus.BOARDING) {
			InvalidFlightInstanceStatusFault fault = new InvalidFlightInstanceStatusFault();
			fault.getExpectedStatus().add(StatusEnumType.BOARDING);
			fault.setCurrentStatus(StatusEnumType.valueOf(flightInstance.getStatus().name()));
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": Invalid status for boarding operation");
			throw new InvalidFlightInstanceStatusFault_Exception(flightInstanceFlightNumber + flightInstanceDeptDate, fault);
		}
			
		Sol4PassengerReader passengerReader;

		try {
			passengerReader = (Sol4PassengerReader) flightInstance.getPassengerReader(passengerName);
		} catch (MalformedArgumentException e) {
			//Never happens as already checked passengerName!=null
			throw new InvalidPassengerNameFault_Exception(passengerName, new InvalidPassengerNameFault());
		}
		
		if(passengerReader == null){
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": " 
					+ "passenger " + passengerName + "not registered on this flightInstance");
			throw new InvalidPassengerNameFault_Exception(passengerName, new InvalidPassengerNameFault());
		}
		if(passengerReader.getSeat() == null){
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": passenger "
					+ passengerName + "couldn't be boarded as was not checked in");
			throw new PassengerNotCheckedInFault_Exception(passengerName, new PassengerNotCheckedInFault());
		}
		
		passengerReader.setBoarded(true);
		LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": passenger "
					+ passengerName + " boarded");
	}
	
	/**
	 * @category SOAPServiceOperation
	 */
	@Override
	public void startBoarding(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate)
			throws InvalidFlightInstanceFault_Exception,
			InvalidFlightInstanceStatusFault_Exception {
		Sol4FlightInstanceReader flightInstanceReader = getFlightInstance(flightInstanceFlightNumber, flightInstanceDeptDate);
		LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": requested boarding operation");
		if(flightInstanceReader.getStatus() == FlightInstanceStatus.BOARDING){
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": now boarding");
		} else if(flightInstanceReader.getStatus() == FlightInstanceStatus.CHECKINGIN) {
			flightInstanceReader.setFlightInstanceStatus(FlightInstanceStatus.BOARDING);
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": already boarding");
		} else {
			InvalidFlightInstanceStatusFault fault = new InvalidFlightInstanceStatusFault();
			fault.getExpectedStatus().add(StatusEnumType.CHECKINGIN);
			fault.getExpectedStatus().add(StatusEnumType.BOARDING);
			fault.setCurrentStatus(StatusEnumType.valueOf(flightInstanceReader.getStatus().name()));
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ":" +
					"status of flight instance cannot pass from " + flightInstanceReader.getStatus()
					+ " to BOARDING");
			throw new InvalidFlightInstanceStatusFault_Exception(flightInstanceFlightNumber + "@" + flightInstanceDeptDate, fault);
		}
	}
	
	/**
	 * @category Getter
	 * @param flightInstanceFlightNumber
	 * @param flightInstanceDeptDate
	 * @return  valid flightInstance
	 * @throws InvalidFlightInstanceFault_Exception if invalid parameters or no such flightInstance
	 */
	private Sol4FlightInstanceReader getFlightInstance(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate) throws InvalidFlightInstanceFault_Exception{
		
		if(flightInstanceFlightNumber == null | flightInstanceDeptDate == null){
			LOGGER.info("getFlightInstance: null arguments in input");
			throw new InvalidFlightInstanceFault_Exception(
					"null parameters", new InvalidFlightInstanceFault());
		}
		Sol4FlightInstanceReader flightInstance = null;
		try {
			flightInstance=(Sol4FlightInstanceReader) _flightMonitor.getFlightInstance(
					flightInstanceFlightNumber, flightInstanceDeptDate.toGregorianCalendar());
		} catch (MalformedArgumentException e) {
			LOGGER.info("getFlightInstance: invalid arguments (null or malformed)");
			throw new InvalidFlightInstanceFault_Exception(
					"getFlightInstance: invalid arguments (null or malformed)", new InvalidFlightInstanceFault());
		}
		
		if(flightInstance == null){
			LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": no such flight instance");
			throw new InvalidFlightInstanceFault_Exception(
					flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": no such flight instance", 
					new InvalidFlightInstanceFault());
		}
		return flightInstance;
	}
	
	/**
	 * @category SOAPServiceOperation
	 */
	@Override
	public List<Passenger> getBoardedPassengerList(
			String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate)
			throws InvalidFlightInstanceFault_Exception {

		Set<PassengerReader> passengerReaders = getFlightInstance(
						flightInstanceFlightNumber,
						flightInstanceDeptDate).getPassengerReaders(null);
		LOGGER.info(flightInstanceFlightNumber + "@" + flightInstanceDeptDate + ": requested boardied passenger list");
		List<Passenger> boardedPassengers = new ArrayList<Passenger>();
		for (PassengerReader passengerReader : passengerReaders) {
			if(passengerReader.boarded()){
				Passenger passenger = new Passenger();
				passenger.setName(passengerReader.getName());
				passenger.setSeat(passengerReader.getSeat());
				boardedPassengers.add(passenger);
			}
		}
		
		return boardedPassengers;
	}
	
	@Override
	public void checkIn(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate, String passengerName,
			Holder<String> seat, Holder<String> gate, Holder<BigInteger> delay)
			throws InvalidFlightInstanceFault_Exception,
			InvalidFlightInstanceStatusFault_Exception,
			InvalidPassengerNameFault_Exception {
		// Not implemented as instructed
		
	}
	
	@Override
	public void setFlightInstanceGate(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate, String gate)
			throws InvalidFlightInstanceFault_Exception {
		// Not implemented as instructed
		
	}
	@Override
	public void setFlightInstanceDelay(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate, BigInteger delay)
			throws InvalidFlightInstanceFault_Exception {
		// Not implemented as instructed
		
	}
	@Override
	public void cancelFlightInstance(String flightInstanceFlightNumber,
			XMLGregorianCalendar flightInstanceDeptDate)
			throws InvalidFlightInstanceFault_Exception {
		// Not implemented as instructed
		
	}

}
