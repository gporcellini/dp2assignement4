package it.polito.dp2.FDS.sol4.server;

import java.util.concurrent.Executors;

import javax.xml.ws.Endpoint;

import it.polito.dp2.FDS.FlightMonitor;
import it.polito.dp2.FDS.FlightMonitorException;
import it.polito.dp2.FDS.FlightMonitorFactory;
import it.polito.dp2.fds.wsdl.fdscontrol.FDSControl;
import it.polito.dp2.fds.wsdl.fdsinfo.FDSInfo;

public class FDSControlServer {
	private static final int THREAD_POOL_SIZE = 10;
	private static final String CONTROL_SERVICE_URL = "http://localhost:7070/fdscontrol";
	private static final String INFO_SERVICE_URL = "http://localhost:7071/fdsinfo";

//this is the publisher
	public static void main(String args[]){
		try {
			//Initialize the data
			System.setProperty("it.polito.dp2.FDS.FlightMonitorFactory", "it.polito.dp2.FDS.Random.FlightMonitorFactoryImpl");
			FlightMonitorFactory factory = FlightMonitorFactory.newInstance();
			FlightMonitor oldFlightMonitor = factory.newFlightMonitor();
			if(oldFlightMonitor == null){
				System.out.println("Error input FlightMonitor was null");
				return;
			}
			Sol4FlightMonitor flightMonitor = Sol4FlightMonitor.getInstance(oldFlightMonitor);
			
			if(flightMonitor == null){
				System.out.println("Error instantiating new FlightMonitor");
				return;
			}
			//Publish FDSControl
			FDSControl fdsControl = new FDSControlImpl(flightMonitor);
			Endpoint controlEndpoint = Endpoint.create(fdsControl);
			controlEndpoint.setExecutor(Executors.newScheduledThreadPool(THREAD_POOL_SIZE));
			controlEndpoint.publish(CONTROL_SERVICE_URL);
			
			//Publish FDSInfo
			FDSInfo fdsInfo = new FDSInfoImpl(flightMonitor);
			Endpoint infoEndpoint = Endpoint.create(fdsInfo);
			infoEndpoint.setExecutor(Executors.newScheduledThreadPool(THREAD_POOL_SIZE));
			infoEndpoint.publish(INFO_SERVICE_URL);
		} catch (FlightMonitorException e) {
			// Never happens as input should be reliable
			e.printStackTrace();
		}
	}
}
