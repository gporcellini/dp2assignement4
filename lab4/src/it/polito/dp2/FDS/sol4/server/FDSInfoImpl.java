package it.polito.dp2.FDS.sol4.server;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.FDS.Aircraft;
import it.polito.dp2.FDS.FlightInstanceReader;
import it.polito.dp2.FDS.FlightInstanceStatus;
import it.polito.dp2.FDS.FlightReader;
import it.polito.dp2.FDS.MalformedArgumentException;
import it.polito.dp2.FDS.PassengerReader;
import it.polito.dp2.FDS.Time;
import it.polito.dp2.fds.wsdl.fdsinfo.FDSInfo;
import it.polito.dp2.fds.wsdl.fdsinfo.InvalidParametersFault;
import it.polito.dp2.fds.wsdl.fdsinfo.InvalidParametersFault_Exception;
import it.polito.dp2.fds.xsd.fdsinfo.AircraftType;
import it.polito.dp2.fds.xsd.fdsinfo.FlightInstanceType;
import it.polito.dp2.fds.xsd.fdsinfo.FlightType;
import it.polito.dp2.fds.xsd.fdsinfo.PassengerType;
import it.polito.dp2.fds.xsd.fdsinfo.StatusEnumType;

@WebService(wsdlLocation="wsdl/FDSInfo.wsdl",
targetNamespace="http://dp2.polito.it/FDS/wsdl/FDSInfo",
serviceName="FDSInfoService",
portName="FDSInfoServiceSOAPPort",
endpointInterface="it.polito.dp2.fds.wsdl.fdsinfo.FDSInfo")
public class FDSInfoImpl implements FDSInfo{
	private Sol4FlightMonitor _flightMonitor;

	public FDSInfoImpl(Sol4FlightMonitor flightMonitor) {
		_flightMonitor = flightMonitor;
	}

	@Override
	public FlightType getFlight(String flightNumber)
			throws InvalidParametersFault_Exception {
		try {
			return toFlightType(_flightMonitor.getFlight(flightNumber));
		} catch (MalformedArgumentException e) {
			throw new InvalidParametersFault_Exception(flightNumber, new InvalidParametersFault());
		}
	}

	@Override
	public void getFlights(String deptIATAcode, String destIATAcode,
			XMLGregorianCalendar deptTime, int pageNumber, int resultsPerPage,
			Holder<List<FlightType>> flight, Holder<Boolean> morePages)
			throws InvalidParametersFault_Exception {
		try {
			int startIndex = pageNumber * resultsPerPage ;
			if(startIndex < 0){
				throw new InvalidParametersFault_Exception("pageNumber*resultsPerPage must be >= 0", new InvalidParametersFault());
			}
			
			List<FlightReader> flightReaders = _flightMonitor.getFlights(deptIATAcode,
					destIATAcode, 
					deptTime == null ? null : new Time(deptTime.getHour(), deptTime.getMinute()));
			
			flight.value = new ArrayList<FlightType>();
			if(startIndex < flightReaders.size()){//start index in boundaries
				int endIndex = startIndex + resultsPerPage;
				if(endIndex >= flightReaders.size()){//end index in boundaries
					morePages.value = false;
					endIndex=flightReaders.size();
				} else {
					morePages.value = true;
				}
				for (FlightReader flightReader : flightReaders.subList(startIndex, endIndex))
					flight.value.add(toFlightType(flightReader));
			} else {
				morePages.value = false;
			}
		} catch (IllegalArgumentException | MalformedArgumentException e) {
			throw new InvalidParametersFault_Exception("Invalid Parameters", new InvalidParametersFault());
		}
	}
	
	@Override
	public FlightInstanceType getFlightInstance(String flightNumber,
			XMLGregorianCalendar deptDate)
			throws InvalidParametersFault_Exception {
		try {
			return toFlightInstanceType(
					_flightMonitor.getFlightInstance(flightNumber, deptDate == null ? null : deptDate.toGregorianCalendar()));	
		} catch (MalformedArgumentException e) {
			throw new InvalidParametersFault_Exception("Invalid Parameters", new InvalidParametersFault());
		}
	}

	@Override
	public void getFlightInstances(String flightNumber,
			XMLGregorianCalendar deptDate, StatusEnumType status,
			int pageNumber, int resultsPerPage,
			Holder<List<FlightInstanceType>> flightInstance,
			Holder<Boolean> morePages) throws InvalidParametersFault_Exception {
		try {
			int startIndex = pageNumber * resultsPerPage ;
			if(startIndex < 0){
				throw new InvalidParametersFault_Exception("pageNumber*resultsPerPage must be >= 0", new InvalidParametersFault());
			}
			
			List<FlightInstanceReader> flightInstanceReaders =
					_flightMonitor.getFlightInstances(flightNumber,
							deptDate == null ? null : deptDate.toGregorianCalendar(), 
							status == null ? null: FlightInstanceStatus.valueOf(status.name()));
			
			flightInstance.value = new ArrayList<FlightInstanceType>();
			if(startIndex < flightInstanceReaders.size()){//start index in boundaries
				int endIndex = startIndex + resultsPerPage;
				if(endIndex >= flightInstanceReaders.size()){//end index in boundaries
					morePages.value = false;
					endIndex=flightInstanceReaders.size();
				} else {
					morePages.value = true;
				}
				for (FlightInstanceReader flightInstanceReader : flightInstanceReaders.subList(startIndex, endIndex))
					flightInstance.value.add(toFlightInstanceType(flightInstanceReader));
			} else {
				morePages.value = false;
			}
		} catch (MalformedArgumentException e) {
			throw new InvalidParametersFault_Exception("Invalid Parameters", new InvalidParametersFault());
		}
	}
	
	/**
	 * @category Converter
	 * @param flightReader
	 * @return
	 * @throws InvalidParametersFault_Exception
	 */
	private FlightType toFlightType(FlightReader flightReader) throws InvalidParametersFault_Exception{
		if(flightReader == null) return null;
		FlightType flightType = new FlightType();
		flightType.setNumber(flightReader.getNumber());
		flightType.setDeptIATAcode(flightReader.getDepartureAirport());
		flightType.setDestIATAcode(flightReader.getDestinationAirport());
		try {
			flightType.setDeptTime(	DatatypeFactory.newInstance().newXMLGregorianCalendarTime(
					flightReader.getDepartureTime().getHour(), 
					flightReader.getDepartureTime().getMinute(), 
					0, 
					0));
		} catch (DatatypeConfigurationException e) {
			 /*this execption should never be raised as input source should be reliable*/ 
			throw new InvalidParametersFault_Exception("Error in server database", new InvalidParametersFault());
		};
				
		return flightType;
	}
	
	/**
	 * @category Converter
	 * @param flightInstanceReader
	 * @return
	 * @throws InvalidParametersFault_Exception
	 */
	private FlightInstanceType toFlightInstanceType(FlightInstanceReader flightInstanceReader) throws InvalidParametersFault_Exception{
		if(flightInstanceReader == null) return null;
		FlightInstanceType flightInstanceType = new FlightInstanceType();
		flightInstanceType.setAircraftId(flightInstanceReader.getAircraft().model);
		flightInstanceType.setFlightNumber(flightInstanceReader.getFlight().getNumber());
		flightInstanceType.setDelay(BigInteger.valueOf(flightInstanceReader.getDelay()));
		flightInstanceType.setGate(flightInstanceReader.getDepartureGate());
		try {
			flightInstanceType.setDeptDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(flightInstanceReader.getDate()));
		} catch (DatatypeConfigurationException e) {
			/*should never happen as my db should be reliable*/
			throw new InvalidParametersFault_Exception("Error in server database", new InvalidParametersFault());
		}
		flightInstanceType.setStatus(StatusEnumType.valueOf(flightInstanceReader.getStatus().name()));
		for (PassengerReader passengerReader : flightInstanceReader.getPassengerReaders(null)) {
			flightInstanceType.getPassenger().add(toPassengerType(passengerReader));
		}
		
		return flightInstanceType;
	}
	
	/**
	 * @category Converter
	 * @param aircraft
	 * @return
	 */
	@SuppressWarnings("unused")
	private AircraftType toAiracraftType(Aircraft aircraft){
		if(aircraft == null) return null;
		AircraftType aircraftType = new AircraftType();
		aircraftType.setModel(aircraft.model);
		for (String seat : aircraft.seats) 
			aircraftType.getSeat().add(seat);
		
		return aircraftType;
	}
	
	/**
	 * @category Converter
	 * @param passengerReader
	 * @return
	 */
	private PassengerType toPassengerType(PassengerReader passengerReader){
		if(passengerReader == null) return null;
		PassengerType passengerType = new PassengerType();
		passengerType.setName(passengerReader.getName());
		passengerType.setSeat(passengerReader.getSeat());
		passengerType.setBoarded(passengerReader.boarded());
		
		return passengerType;
	}

	@Override
	public void getAircrafts(int pageNumber, int resultsPerPage,
			Holder<List<AircraftType>> aircraft, Holder<Boolean> morePages) {
		// TODO Auto-generated method stub
		
	}

}
